export const environment = {
  production: true,
  serverUrl: 'https://alkoukhapp.kreate-soft.com/api',
  appUrl: 'https://alkoukhapp.kreate-soft.com',
  appImageUrl: 'https://alkoukhapp.kreate-soft.com/assets/imgs/ionshop.png',
  appBannerUrl: 'http://alkoukhassets.kreate-soft.com/',
  appAssetUrl: 'http://alkoukhassets.kreate-soft.com/',
  appId: 'vvOE7MQvmV',
  fbId: 'not_set',
  stripePublicKey: 'not_set',
  androidHeaderColor: '#ff9d0c',
  defaultLang: 'ar',
  isFirebase: true,
  firebase: {
    apiKey: "AIzaSyB86_-7jSJMl8Ikaz2Gv4_61VATz3ED2_o",
    authDomain: "max-store-app.firebaseapp.com",
    databaseURL: "https://max-store-app.firebaseio.com",
    projectId: "max-store-app",
    storageBucket: "max-store-app.appspot.com",
    messagingSenderId: "997651794177",
    appId: "1:997651794177:web:f4ac216a72f0b5a280b300",
    measurementId: "G-EGFVD1MZLK"
  },
};

export const environment_old = {
  production: true,
  serverUrl: 'https://admin.tryion.shop/parse',
  appUrl: 'https://tryion.shop',
  appImageUrl: 'https://tryion.shop/assets/imgs/ionshop.png',
  appId: 'JrWy7sUKLL',
  fbId: '2081754298556977',
  stripePublicKey: 'pk_test_MVWLbqwGEHDv5J9MkdN4i7Ot',
  androidHeaderColor: '#5D67D4',
  defaultLang: 'ar',
};
