import { Injectable } from '@angular/core';
import * as Parse from 'parse';

@Injectable({
  providedIn: 'root'
})
export class SubCategory {

  constructor() {
    
  }

  load(params: any = {}) {
    return [];
  }

  count(params: any = {}){
    return 0;
  }

  id;
  name;
  status;
  category;
  imageThumb;
  slug;
}