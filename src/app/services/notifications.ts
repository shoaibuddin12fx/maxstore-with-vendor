import { Injectable } from '@angular/core';
import * as Parse from 'parse';

@Injectable({
  providedIn: 'root'
})
export class Notification {

  constructor() {
     }

  load(params: any = {}) { 

    return [];
  }

  static getInstance() {
    return this;
  }

  createdAt;
  message;

}