import { Injectable } from '@angular/core';
import * as Parse from 'parse';
import { FirebaseService } from './fb/firebase.service';

@Injectable({
  providedIn: 'root'
})
export class Order {

  id;
  number;
  customer;
  firstItem;
  items;
  subtotal;
  total;
  shipping;
  card;
  paymentMethod;
  contactNumber;
  status;

  
  constructor(public firebaseService: FirebaseService) {
    
  }
  prettyItems() {
    return this.items.map((item: any) => item.name).join(', ');
  }

  load(): Promise<any[]> {

    return new Promise( async resolve => {
      
      let uid = this.firebaseService.returnUid();
      if(!uid){
        resolve([]);
      }else{
        let orders = await this.firebaseService.getMyOrderList(uid)
        resolve(orders);
      }

    })
    
  }

  loadOne(id: string): Promise<any> {
    return this.firebaseService.getOrderDetail(id)
  }

}