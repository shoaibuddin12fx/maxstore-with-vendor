import { Injectable } from '@angular/core';
import * as Parse from 'parse';

@Injectable({
    providedIn: 'root'
})
export class Category {

    constructor() {

    }

    static getInstance() {
        return this;
    }

    load(params: any = {}) {

        

        return [];
    }

    id
    name
    status
    slug
    imageThumb
    subCategoryCount
}
