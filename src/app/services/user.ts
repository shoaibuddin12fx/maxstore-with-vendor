import { Injectable } from '@angular/core';
import * as Parse from 'parse';
import { Events } from '@ionic/angular';
import { environment } from 'src/environments/environment';
import { FbUserService } from './fb/fb-user.service';
import { Facebook } from '@ionic-native/facebook/ngx';
import * as firebase from 'firebase/app';
import { FirebaseService } from './fb/firebase.service';

@Injectable({
  providedIn: 'root'
})
export class User {

  name;
  email;
  phone;
  username;
  password;
  authData;
  photo;



  constructor(public fbUser: FbUserService, public fb: FirebaseService, public facebook: Facebook) {

  }

  public async setFcmToken(){
    this.fb.setUserToken();
  }

  public async anonymousLogin(uuid) {

    return new Promise(resolve => {

      let IMEI = uuid;
      let password = 'RXJX_r45x';
      const tObj = { email: `${IMEI}@mailinator.com`, username: IMEI, password: password }
      this.signIn(tObj).then(user => {
        console.log(user);
        resolve(user);
      })
        .catch(async err => {
          console.log("NO USER -->");
          let user = await this.create(tObj);
          resolve(user);
        });
    })



    // if(!auth){
    //   return this.create(tObj);
    // }
    // return auth;

  }

  loginViaFirebasePhone(uid, number) {

    return new Promise(resolve => {
      this.fbUser.loginVIaPhone(uid, number);
      resolve()
    })

  }








  static getInstance() {
    return this;
  }

  getCurrent() {
    // if firebase 
    return this.fbUser.getAuthUser();
  }



  isLoggedInViaPassword() {
    let auth = this.fbUser.getAuthUser();
    this.authData = auth as any;
    return this.authData;
  }

  // linkWith(provider: string, authData: any = {}): Promise<User> {
  //   const user: any = new User;
  //   return user._linkWith(provider, authData);
  // }

  loginViaFacebook(): Promise<any> {
    return new Promise(resolve => {
      this.facebook.login(['email'])
        .then(response => {
          const facebookCredential = firebase.auth.FacebookAuthProvider
            .credential(response.authResponse.accessToken);

          firebase.auth().signInWithCredential(facebookCredential)
            .then(success => {
              console.log("Firebase success: " + JSON.stringify(success));
            });

        }).catch((error) => { console.log(error) }); 
    })
  }

  isFacebookLinked(): boolean {
    return
    // Parse.FacebookUtils.isLinked(Parse.User.current());
  }

  async signIn(data: any = {}): Promise<any> {

    return new Promise(resolve => {
      this.fbUser.loginUser(data.email, data.password).then(res => {
        console.log(res)
        this.authData = res as any;
        resolve(res)
      }, error => {
        console.log(error)
        resolve(null)
      })
    })



  }

  async create(data: any = {}): Promise<any> {

    return new Promise(resolve => {
      this.fbUser.signupUser(data.email, data.password, data.username, data.phone).then(res => {
        console.log(res)
        this.authData = res as any;
        resolve(res)
      }, error => {
        console.log(error)
        resolve(null)
      })
    })

  }

  recoverPassword(email) {
    return this.fbUser.resetPassword(email);
  }

  logout() {
    return this.fbUser.logoutUser();
  }

  params: any;
  save(obj: any) {
    this.params = obj;
  }

}
