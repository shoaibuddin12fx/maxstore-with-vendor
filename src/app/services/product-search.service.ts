import { Injectable } from '@angular/core';
import { FirebaseService } from './fb/firebase.service';
import { Events } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class ProductSearchService {
  products: any;

  constructor(
    private firebaseService: FirebaseService,
    private events: Events
  ) { 
    this.events.subscribe('product:searched', (value) => {
      console.log(value);
      this.filterProductsOnSearch(value);
    });
  }

  getAllProducts(): Promise<any[]> {
    return new Promise( async resolve => {
      console.log('ON Product Search Page');
      this.products = await this.firebaseService.getItemList();
      resolve(this.products);
    })
    
  }

  filterProductsOnSearch(text) {
    console.log(text);
    console.log(this.products);
    let filteredProducts = this.products.filter(x => x.name === text.searchString);
    console.log(filteredProducts);
  }
}
