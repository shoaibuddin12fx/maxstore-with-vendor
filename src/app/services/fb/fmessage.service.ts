import { Injectable } from '@angular/core';
import { FCM } from '@ionic-native/fcm/ngx';
import { AlertsService } from '../alerts.service';


@Injectable({
  providedIn: 'root'
})
export class FmessageService {

  constructor(private fcm: FCM, private alerts: AlertsService,) { 
    
  }

  async initialize(){

    this.fcm.subscribeToTopic('all');

    await this.getFcmToken();
    this.fcm.onNotification().subscribe(data => {
      if(data.wasTapped){
        console.log("Received in background");
      } else {
        console.log("Received in foreground");

        this.alerts.presentToast(data.title + ' : ' + data.body );

      };
    });

    this.fcm.onTokenRefresh().subscribe(token => {
      // backend.registerToken(token);
    });



  }

  getFcmToken(){

    return new Promise( resolve => {
      this.fcm.getToken().then(token => {
        localStorage.setItem('token', token);
        resolve(token);
      });
    })

  }

  isFcmHasPermission(){

    return new Promise( resolve => {

      this.fcm.hasPermission().then(hasPermission => {
        if (hasPermission) {
          console.log("Has permission!");
          resolve(true);
        }else{
          resolve(false)
        }
      })

    })

  }

  clearAllNotification(){
    this.fcm.clearAllNotifications();
  }

  unsubscribeTopics(){
    this.fcm.unsubscribeFromTopic('marketing');
  }


}
