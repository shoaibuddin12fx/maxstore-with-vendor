import { Injectable } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { AngularFirestore, AngularFirestoreModule } from 'angularfire2/firestore';

import { Firebase } from '@ionic-native/firebase/ngx';
import * as firebase from 'firebase/app';
import 'firebase/auth';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook/ngx';
import { UserControlService } from '../user-control.service';
import { Preference } from '../preference';
import { Observable, combineLatest, of } from 'rxjs'

@Injectable({
  providedIn: 'root'
})
export class FirebaseService {

  product_id: Array<number> = [];
  url: any;
  cart: any;
  params: any;
  orderLists: any;
  public ref: any;
  productsList: any;
  customerList: any;
  public orderList: any;

  total: number = 0;
  proqty: Array<number> = [];
  getSecKey: any;
  users: any;

  public categoryslides: any;
  public banners: any;
  public vendors: any;
  public fireAuth: any;
  public restaurantUserInfo: any;
  public restaurants: any;
  public restaurantCategory: any;
  public category: any;
  public restaurantItems: any;
  public featuredItems: any;
  public items: any;
  public currentUser: any;
  public userAddressList: any;
  public appDetails: any;

  public cityName: any;
  public cityDistrictName: any;
  public streetName: any;
  public apartmentOfficeName: any;
  public categorizedOrders: any;
  public favoriteItem: any;
  public favoriteItemList: any;
  public chats: any;
  public userChatList: any;
  public allChoosenItems: any;

  public hotelCords: any;

  _PLATFORM: any;

  form: any;

  errorMessage: any;
  customer: any;
  restaurantName: any;

  public signupForm;
  loading: any;

  _USER: any = {
    isLogin: false,
  };

  constructor(
    public afs: AngularFirestore,
    public facebook: Facebook,
    public alertCtrl: AlertController,
    public userControlService: UserControlService,
    public pref: Preference,
    private fb: Firebase
  ) {
    this.cart = { line_items: [], extraOptions: [] };

    this.currentUser = firebase.auth().currentUser;

    console.log(this.currentUser);

    this.fireAuth = firebase.auth();

    this.restaurantUserInfo = firebase.database().ref('/users');

    this.vendors = firebase.database().ref('/vendor');

    this.restaurants = firebase.database().ref('/restaurants');

    this.banners = firebase.database().ref('/slider');

    this.categoryslides = firebase.database().ref('/category');

    this.restaurantCategory = firebase.database().ref('/category');

    this.items = firebase.database().ref('/items');

    this.cityName = firebase.database().ref('/city');

    this.cityDistrictName = firebase.database().ref('/districts');

    this.streetName = firebase.database().ref('/streets');

    this.apartmentOfficeName = firebase.database().ref('/apartments');

    this.orderList = firebase.database().ref('/orders');

    this.categorizedOrders = firebase.database().ref('/categorizedOrders');

    this.chats = firebase.database().ref('/chats');

    this.allChoosenItems = firebase.database().ref('/items');

    this.hotelCords = firebase.database().ref('/cordItems');

    this.appDetails = firebase.database().ref('/about');
  }

  setFirebaseLanguage(code){
    firebase.auth().languageCode = code;
  }

  isAuth() {
    return firebase.auth();
  }

  returnUid(){
    return firebase.auth().currentUser.uid;
  }

  logout(){
    return firebase.auth().signOut()
  }

  getTimeStamp() {
    return firebase.database.ServerValue.TIMESTAMP;
  }

  getRestaurantUserProfile(id): any {
    return this.restaurantUserInfo.child(id);
  }

  setUserToken(){

    let token = localStorage.getItem('fcm_token');
    let uid = this.returnUid();
    
    this.restaurantUserInfo.child(uid).update({
      token: token
    });
    

  }

  getRestaurantsList(): any {
    console.log(this.restaurants);
    return this.restaurants;
  }

  getCategoryList(): Promise<any[]> {
    return new Promise( resolve => {
      this.restaurantCategory.orderByChild('parent_category_id').equalTo('').on('value', snapshot => {
        resolve(this.compileArray(snapshot));
      });
    });
  }

  getCategoryListBtId(id): Promise<any>{
    return new Promise( resolve => {
      console.log(id);
      this.restaurantCategory.child(id).on('value', snapshot => {
        resolve(this.compile(snapshot));
      });
    });
  }


  getSubCategoryListOfCategory(id): Promise<any[]>{
    return new Promise( resolve => {
      console.log(id);
      this.restaurantCategory.orderByChild('parent_category_id').equalTo(id).on('value', snapshot => {
        resolve(this.compileArray(snapshot));
      });
    })
  }



  getVendorList(): Promise<any[]> {
    return new Promise( resolve => {
      this.vendors.on('value', snapshot => {
        resolve(this.compileArray(snapshot));
      });
    });
  }

  getVendorListBtId(id): Promise<any>{
    return new Promise( resolve => {
      this.vendors.child(id).on('value', snapshot => {
        resolve(this.compile(snapshot));
      });
    });
  }

  getOrderDetail(id) {
    // return this.orderList.child(id);
    return new Promise( resolve => {
      this.orderList.child(id).on('value', snapshot => {
        resolve(this.compile(snapshot));
      });
    });
  }

  getBannersList(): Promise<any[]> {
    return new Promise( resolve => {
      this.banners.on('value', snapshot => {
        resolve(this.compileArray(snapshot));
      });
    });
  }

  GetCategoryList(): Promise<any[]>{
    return new Promise(resolve => {
      this.categoryslides.on('value', snapshot =>{
        resolve(this.compileArray(snapshot));
      })
    })
  }


  getItemList(pagination = ''): Promise<any[]> {
    return new Promise( resolve => {
      this.items.orderByChild('status').equalTo('Active').on('value', snapshot => {
        resolve(this.compileArray(snapshot));
      });
    });
  }

  getfeturedList(): Promise<any[]> {
    return new Promise( resolve => {

      this.items.orderByChild('feature').equalTo('true').on('value', snapshot => {
        resolve(this.compileArray(snapshot));
      });

    });
  }

  loadMyOrders(): Promise<any[]> {
    return new Promise( resolve => {

      let uid = firebase.auth().currentUser.uid;
      this.orderList.orderByChild('user_id').equalTo(uid).on('value', snapshot => {
        resolve(this.compileArray(snapshot));
      });
    })
  }

  compileArray(snapshot){
    let items = [];
    snapshot.forEach( snap => {
      items.push(this.compile(snap));
    });

    return items;
  }

  compile(snapshot){
    let v = snapshot.val();
    v.id = snapshot.key;
    return v;
  }

  verificationSMSsendFunction(phone) {
    return new Promise((resolve, reject) => {
      this.fb
        .verifyPhoneNumber('+' + phone.countryCode + phone.phoneNumber, 60)
        .then((responseInfo) => {
          resolve(responseInfo);
        })
        .catch((err) => {
          console.log(err);
          reject(err);
        });
    });
  }

  getRestaurantCategoryLists(id) {
    console.log(id);
    this.category = this.restaurantCategory
      .orderByChild('res_name')
      .equalTo(id);
    return this.category;
  }

  getItemListBtCategoryId(id, limit = 0): Promise<any[]> {

    return new Promise( resolve => {
      
      let init = this.restaurantItems = this.items.orderByChild('categories').equalTo(id);
      let query;
      if(limit > 0){
        query = init.limitToFirst(limit);
      } else {
        query = init;
      }

      query.on('value', snapshot => {
        console.log(snapshot.val());
        resolve(this.compileArray(snapshot));
      });
    })


  }

  getItemListBySubCategoryId(id): Promise<any[]> {

    return new Promise( resolve => {
      this.restaurantItems = this.items.orderByChild('sub_category').equalTo(id)
      .on('value', snapshot => {
        console.log(snapshot.val());
        resolve(this.compileArray(snapshot));
      });
    })


  }

  getFeaturedItemByCategoryId(id, limit): Promise<any[]> {
    console.log(id);

    return new Promise( resolve => {
      this.featuredItems = this.items.orderByChild('categories').equalTo(id).limitToFirst(10)
      .on('value', snapshot => {
        console.log(snapshot.val());
        resolve(this.compileArray(snapshot));

      });

    })


  }

  getFavoriteItemByIds(ids: any[]): Promise<any[]> {

    return new Promise( async resolve => {

      var items = [];

      if(ids.length == 0){
        return items;
      }

      var cList = [];
      for(var i = 0; i < ids.length; i++){
        let it = await this.getItemDetail(ids[i]);
        items.push(it);

      }

      resolve(items);


      

    })


  }



  setPlatform(data) {
    this._PLATFORM = data;
  }

  getItemDetail(id): any {

    return new Promise( resolve => {

      this.items.child(id).on('value', snapshot => {
        console.log(snapshot.val());

        let item = snapshot.val();
        item['id'] = snapshot.key;

        resolve(item);

      });

    });

  }

  checkRegisteredEmail(email: string) {
    return new Promise((resolve, reject) => {
      firebase
        .auth()
        .fetchSignInMethodsForEmail(email)
        .then((responseInfo) => {
          if (responseInfo.length > 0) {
            resolve(responseInfo);
          } else {
            reject(false);
          }
        });
    });
  }

  getItemExtraOptionsDetail(id) {
    return this.items.child(id).child('extraOptions');
  }

  getCurrentUserAddresses(uid) {
    this.userAddressList = this.restaurantUserInfo
      .child(uid)
      .child('addresses');

    return this.userAddressList;
  }

  getCityName(): Promise<any[]> {
    // console.log(this.cityName);
    // return this.cityName;
    return new Promise(resolve => {

      this.cityName.on('value', snapshot => {
        console.log(snapshot.val());

        let cities = [];

        snapshot.forEach( snap => {
          cities.push({
            id: snap.key,
            name: snap.val().name,
            shippingfee: snap.val().shippingfee,
            status: snap.val().status
          })

        });

        resolve(cities);

      });

    });
  }

  getCityDistrictName(): Promise<any[]> {
    return new Promise(resolve => {

      this.cityDistrictName.on('value', snapshot => {

        let district = [];

        snapshot.forEach( snap => {
          district.push({
            id: snap.key,
            city: snap.val().city,
            name: snap.val().name,
            status: snap.val().status
          })

        });

        resolve(district);

      });

    });
  }

  getAppDetails(): Promise<any[]> {


    return new Promise(resolve => {

      this.appDetails.child('-M9IziUyofL_5VQAyARS').on('value', snapshot => {
        console.log(snapshot.val());

        let item = snapshot.val();
        item['id'] = snapshot.key;

        resolve(item);

      });

    });
  }
  sendEmailConfirm(user) {
    return new Promise((resolve, reject) => {
      firebase
        .auth()
        .currentUser.sendEmailVerification()
        .then(() => {
          resolve(
            user.name +
              'Welcome to Multi Vendor System ,verification link has been sent into your inbox'
          );
        })
        .catch((err) => {
          reject(err);
        });
    });
  }

  getStreetName() {
    console.log(this.streetName);
    return this.streetName;
  }

  get user() {
    return this._USER;
  }

  getApartmentOfficeName() {
    return this.apartmentOfficeName;
  }

  saveNewAddress(
    city,
    district,
    street,
    apartmentOffice,
    displayName,
    email,
    phone,
    address,
    uid
  ) {
    return this.restaurantUserInfo
      .child(uid)
      .child('addresses')
      .push({
        city: city,
        district: district,
        street: street,
        apartmentOffice: apartmentOffice,
        displayName: displayName,
        email: email,
        phone: phone,
        address: address,
        uid: uid,
        timeStamp: firebase.database.ServerValue.TIMESTAMP,
        reverseOrder: 0 - Date.now(),
      });
  }

  setCustomerInfo(uid) {
    return new Promise((resolve, reject) => {
      this.userControlService.getUserInfo(uid).once('value', (q) => {
        if (q.val()) {
          this._USER = q.val();
          resolve(this._USER);
        } else {
          reject(
            'In order to login system Customer must first register into system'
          );
        }
      });
    });
  }

  addOrders(obj) {

    return new Promise( resolve => {
      obj['user_id'] = firebase.auth().currentUser.uid;
      obj['createdAt'] = this.getTimeStamp()

      let newOrder = this.orderList.push(obj);
      resolve(newOrder.key);
    })


  }

  addItemReview(obj) {
    return new Promise(resolve => {
      this.items.child(obj.itemId).child('comments').push(obj);
      console.log(obj);
      resolve(true);
    });
  }

  getItemReview(itemId) {
    return new Promise(resolve=> {
      this.items.child(itemId).child('comments').on('value', snapshot => {
          console.log(this.compileArray(snapshot));
          let array = this.compileArray(snapshot);

          let sort = array.sort(function (a, b) {
            return b.createdAt - a.createdAt;
          });

          resolve(sort);


      });
    });
  }
  getUserProfile(id): any {
    return this.restaurantUserInfo.child(id);
  }

  phoneAuth(verifId, code, phone, action = 'login') {
    return new Promise((resolve, reject) => {
      const customerCredentialInfo = firebase.auth.PhoneAuthProvider.credential(
        verifId,
        code
      );
      firebase
        .auth()
        .signInWithCredential(customerCredentialInfo)
        .then((responseInfo: any) => {
          if (action === 'login') {
            this.setCustomerInfo(firebase.auth().currentUser.uid)
              .then(() => {
                resolve('Successfully logged in using phone number');
              })
              .catch((err) => {
                reject(err);
              });
          } else {
            resolve(responseInfo.user);
          }
        })
        .catch((err) => {
          reject(err);
        });
    });
  }

  chargeStripe(token, currency, amount, secret_kay) {
    return false;

  // this.getSecKey = secret_kay;
  //   var headers = new Headers();
  //   var params = new URLSearchParams();

  //   headers.append('Content-Type', 'application/x-www-form-urlencoded');
  //   headers.append('Authorization', 'Bearer ' + secret_kay);
  //   params.append("currency", currency);
  //   params.append("amount",  amount);
  //   params.append("description", "description");
  //   params.append("source", token);
  // 	alert("params-"+JSON.stringify(params));

  // 			return new Promise(resolve => {
  // 			  this.http.post(  'https://api.stripe.com/v1/charges', params, { headers: headers }).map(res => res.json())
  // 				.subscribe(data => {
  // 					alert("DATA"+data);
  // 				  resolve(data);
  // 				});
  // 			});
  }

  addIdToOrder(newOrderKey) {
    return this.orderList.child(newOrderKey).child('id').set(newOrderKey);
  }

  updatePhone(verifId, code, phone) {
    return new Promise((resolve, reject) => {
      const credential = firebase.auth.PhoneAuthProvider.credential(
        verifId,
        code
      );
      firebase.auth().onAuthStateChanged((user: any) => {
        user
          .updatePhoneNumber(credential)
          .then(() => {
            this.user.phone = '+' + phone.countryCode + phone.phoneNumber;
            this.userControlService
              .updateUserInfo(user.uid, { phone: this.user.phone })
              .then(() => {
                resolve('Mobile phone number has been successfully updated');
              });
          })
          .catch((err) => {
            reject(err.message);
          });
      });
    });
  }

  addNewOrdersToEachRestaurantExtra(
    orderKey,
    restaurantKey,
    restaurantName,
    /**extras,*/ order,
    imagess,
    name,
    price,
    productId,
    quantity,
    restaurantId,
    restaurantNames,
    newOrderDetails
  ) {
    console.log(orderKey);
    console.log(restaurantKey);
    console.log(restaurantName);
    console.log(order);

    return this.categorizedOrders.child(order.owner_id).child(orderKey).set({
      addresses: newOrderDetails.addresses,
      customerDetails: newOrderDetails.customerDetails,
      email: newOrderDetails.email,
      items: newOrderDetails.items,
      payments: newOrderDetails.payments,
      status: newOrderDetails.status,
      timeStamp: newOrderDetails.timeStamp,
      total: newOrderDetails.total,
      restaurant_owner_id: order.owner_id,
    });
  }

  registerByPhone(user, verifId, code) {
    return new Promise((resolve, reject) => {
      const customerCredentialInfo = firebase.auth.PhoneAuthProvider.credential(
        verifId,
        code
      );
      firebase
        .auth()
        .signInWithCredential(customerCredentialInfo)
        .then((responseInfo: any) => {
          console.log(responseInfo);

          firebase
            .auth()
            .currentUser.linkWithCredential(
              firebase.auth.EmailAuthProvider.credential(user.email, user.pass)
            )
            .then(() => {
              user.uid = firebase.auth().currentUser.uid;
              user.phone =
                '+' + user.userInput.countryCode + user.userInput.phoneNumber;
              console.log(user);

              this.userControlService.createNewUser(user).then(() => {
                this.setCustomerInfo(user.uid).then(() => {
                  this.sendEmailConfirm(user).then((q) => {
                    resolve(q);
                  });
                });
              });
            })
            .catch((err) => {
              console.log('error link email');
              alert(err);
              reject(err);
            });
        })
        .catch((err) => {
          if (err.code !== 'auth/internal-error') {
            reject(err);
          }
        });
    });
  }

  categorizedRestaurantOrder(orderKey, restaurantKey, owner_id) {
    return this.categorizedOrders.child(owner_id).child(orderKey).update({
      id: orderKey,
    });
  }



  getMyOrderList(id) {
    console.log(id);
    this.orderLists = this.orderList.orderByChild('email').equalTo(id);
    return this.orderLists;
  }

  isAuthenticated() {


    return new Promise( resolve => {
      firebase.auth().onAuthStateChanged( user => {
        resolve(true);
      }, err => {
        resolve(false);
      })
    })
    
  }

  getFavoriteItem(id): any {
    console.log(id);
    console.log(firebase.auth());

    var uid = firebase.auth().currentUser.uid;

    this.favoriteItem = this.restaurantUserInfo
      .child(uid)
      .child('favorites')
      .child(id);
    return this.favoriteItem;
  }

  addToFavorite(data, id) {
    var uid = firebase.auth().currentUser.uid;

    console.log('service');
    console.log(uid);
    console.log(data);

    this.restaurantUserInfo.child(uid).child('favorites').child(id).set({
      product_id: id,
      image: data.image_firebase_url,
      name: data.name,
      price: data.price,
      categories: data.categories,
      available: data.available,
      category: data.category,
      description: data.description,
      stock: data.stock,
      restaurantId: data.restaurantId,
      restaurantName: data.restaurantName,
      market: true,
    });
  }

  removeFavourite(id) {
    console.log(id);
    var uid = firebase.auth().currentUser.uid;

    this.restaurantUserInfo.child(uid).child('favorites').child(id).remove();
  }

  getUserFavouriteList() {
    var uid = firebase.auth().currentUser.uid;

    this.favoriteItemList = this.restaurantUserInfo
      .child(uid)
      .child('favorites');
    return this.favoriteItemList;
  }

  removeFavItem(item) {
    var uid = firebase.auth().currentUser.uid;

    console.log(item.id);

    this.restaurantUserInfo
      .child(uid)
      .child('favorites')
      .child(item.id)
      .remove();
  }

  deleteUserAddress(id) {
    var uid = firebase.auth().currentUser.uid;

    return this.restaurantUserInfo
      .child(uid)
      .child('addresses')
      .child(id)
      .remove();
  }

  getUserChatList(id) {
    console.log(id);

    this.userChatList = this.chats.child(id).child('chat');

    return this.userChatList;
  }

  checkRegisteredMobileNumber(phone) {
    return new Promise((resolve, reject) => {
      this.userControlService
        .getInformationUserBy(
          'phone',
          '+' + phone.countryCode + phone.phoneNumber
        )
        .once('value', (responseInfo) => {
          if (responseInfo.val()) {
            resolve(
              +phone.countryCode +
                phone.phoneNumber +
                ' mobile number already used on Multi Vendor System , please use the other one'
            );
          } else {
            reject(
              '+(' +
                phone.countryCode +
                '-' +
                phone.phoneNumber +
                ') Your agreement with Author will also include the Additional Terms by reference. The Additional Terms shall include, but are not limited to, the terms and policies set out in paragraph 9 below. In order to use the Software, You must accept the Additional Terms'
            );
          }
        });
    });
  }

  addRoom(uid, data, userImage, userName) {
    console.log(data);

    this.chats
      .child(data.owner_id)
      .child(data.id)
      .child('chat')
      .child(uid)
      .child('list')
      .child('-0000')
      .set({
        type: 'join',
        user: 'user',
        message: 'Welcome to restaurant.',
        timeStamp: firebase.database.ServerValue.TIMESTAMP,
        sendDate: '',
      });

    this.chats
      .child(data.owner_id)
      .child(data.id)
      .child('chat')
      .child(uid)
      .update({
        restaurantTitle: data.title,
        restaurantImage: data.firebase_url,
        restaurantOwnerId: data.owner_id,
        timeStamp: firebase.database.ServerValue.TIMESTAMP,
        userImage: userImage,
        userName: userName,
        lastMessage: 'Hello Dear',
      });

    this.chats
      .child(uid)
      .child('chat')
      .child(data.id)
      .child('list')
      .child('-0000')
      .set({
        type: 'join',
        user: 'user',
        message: 'Welcome to restaurant.',
        sendDate: '',
      });

    return this.chats.child(uid).child('chat').child(data.id).update({
      restaurantTitle: data.title,
      restaurantImage: data.firebase_url,
      restaurantOwnerId: data.owner_id,
      userImage: userImage,
      userName: userName,
      lastMessage: 'Hello Dear',
    });
  }

  getAllChooseItems(): any {
    console.log(this.allChoosenItems);
    return this.allChoosenItems;
  }

  getNearestRestaurantItems() {
    var uid = firebase.auth().currentUser.uid;

    return this.hotelCords.child(uid).orderByChild('item_dis');
  }

  get platform() {
    return this._PLATFORM;
  }

  getItemListByVendorID(id): Promise <any[]>{
    return new Promise(resolve => {
      this.restaurantItems =this.items.orderByChild('vendor').equalTo(id)
      .on('value',snapshot => {
        let items = [];

        snapshot.forEach(snap => {
          items.push({
            id: snap.key,
            available: snap.val().available,
            categories: snap.val().categories,
            description: snap.val().description,
            feature: snap.val().feature,
            image: snap.val().image,
            image_firebase_url: snap.val().image_firebase_url,
            name: snap.val().name,
            new_arrival: snap.val().new_arrival,
            percent: snap.val().percent,
            price: snap.val().price,
            saleprice: snap.val().saleprice,
            sort: snap.val().sort,
            status: snap.val().status,
            stock: snap.val().stock,
            sub_category: snap.val().sub_category,
            vendor: snap.val().vendor,
          })
        })
        resolve(items);
      })
    })

  }
}
