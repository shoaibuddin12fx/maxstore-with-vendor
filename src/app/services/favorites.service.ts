import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})
export class FavoritesService {

  favIds: any[] = [];

  constructor(public storage: Storage) { 
    //this.initialize();
  }

  async initialize(){
    this.favIds = await this.getStorage();
  }

  async setIntoFav(id){
    this.favIds = await this.getStorage();
    console.log(this.favIds);
    let findIndex = this.favIds.includes(id);
    if(!findIndex){
      this.favIds.push(id);
      this.setStorage(this.favIds);
    }

  }

  isFavourit(id){
    return this.favIds.includes(id);
  }

  removeFromFav(id){
    let findIndex = this.favIds.indexOf(id);
    console.log(findIndex);
    if(findIndex != -1){
      this.favIds.splice(findIndex, 1);
      this.setStorage(this.favIds);
    }
  }

  setStorage(array){
    this.storage.set("fav", JSON.stringify(array));
  }

  getStorage(): Promise<any[]>{
    return new Promise( async resolve => {
      let fav = await this.storage.get("fav");
      console.log(fav);
      if(!fav){
        this.setStorage([]);
        resolve([]);  
      }else{
        let v = JSON.parse( fav )
        console.log(v);
        resolve(v);
      }
    })
    
  }







}
