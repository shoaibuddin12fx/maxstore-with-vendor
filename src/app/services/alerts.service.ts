import { Injectable } from '@angular/core';
import { Toast } from '@ionic-native/toast/ngx';


@Injectable({
  providedIn: 'root'
})
export class AlertsService {

  constructor(private toast: Toast) { }

  presentToast(msg){
    this.toast.show(msg, '5000', 'bottom').subscribe(
      toast => {
        console.log(toast);
      }
    );
  }
}
