import { Injectable } from '@angular/core';
import * as Parse from 'parse';

@Injectable({
  providedIn: 'root'
})
export class Slide {

  constructor() {
  
  }

  static getInstance() {
    return this;
  }

  image;
  item;
  url;
  sort;
  isActive;
}
