import { Injectable } from '@angular/core';
import { Events } from '@ionic/angular';
import { Preference } from './preference';



export class Cart  {
    
  id: number = Math.random();
  items: any[] = [];
  shipping: number = 0;
  subtotal: number = 0;
  total: number = 0;
  

}


@Injectable({
  providedIn: 'root'
})

export class CartService {

  cart: Cart;

  constructor( public events: Events, public preference: Preference ) {
    this.cart = new Cart();
  }

  get(): Cart{
    return this.cart ? this.cart : new Cart();
  }

  new(): Cart {
    return new Cart();
  }

  empty(): boolean {
    return this.cart.items.length === 0;
  }

  // getOne(): Promise<Cart> {
  //   const query = new Parse.Query(Cart);
  //   query.include('customer');
  //   query.equalTo('customer', Parse.User.current());
  //   return query.first();
  // }

  setShippingFee(number){
    this.cart.shipping = number;
  }

  calculateSubtotal() {
    let total = this.cart.items.reduce((prevVal, item) => {
      let p = parseInt(item.saleprice) > 0 ? item.saleprice : item.price;
      return prevVal + (p * item.qty);
    }, 0);
    this.cart.subtotal = total;
    return total
  }

  calculateTotal() {
    let total = this.cart.items.reduce((prevVal, item) => {
      let p = parseInt(item.saleprice) > 0 ? item.saleprice : item.price;
      return prevVal + (p * item.qty);
    }, 0);
    this.cart.total = total + this.cart.shipping ;
    return total;
  }

  async onAddToCart(item): Promise<any> {

    return new Promise(async resolve => {

      let _cart = await this.get();
      const index = _cart.items.findIndex(itm => itm.id == item.id);

      if (index != -1) {
        item.qty = item.qty;
        _cart.items[index].qty = item.qty;
      } else {
        item.qty = item.qty ? item.qty : 1;
        _cart.items.push(item);
        console.log(item);
      }
      this.preference.cartCount = _cart.items.length;
      this.cart = _cart;
      console.log(this.cart);
      this.calculateSubtotal();
      this.calculateTotal();
      resolve(item);
    });
  }

  async getCartQuantityOfProduct(id){

    let _cart = await this.get();
    const index = _cart.items.findIndex(itm => itm.id == id);

    if (index == -1) {
      return 1;
    } else {
      return _cart.items[index]['qty'];
    }

  }
 
}
