import { Injectable } from '@angular/core';
import * as Parse from 'parse';

@Injectable({
  providedIn: 'root'
})
export class Card {

  constructor() {

  }

  formatBrand() {
    return this.brand.toLowerCase().replace(' ', '_')
  }

  static getInstance() {
    return this;
  }

  load(){
    return [];
  }

  create(params: any = {}) {
    const obj = new Card();
    return obj
  }

  delete(card: Card) {
    return card;
  }

  destroy(){
    return this;
  }
  
  brand
  tripeToken
  rand
  xpMonth
  xpYear
  ast4

}
