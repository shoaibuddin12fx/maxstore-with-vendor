import { Injectable } from '@angular/core';
import * as Parse from 'parse';

@Injectable({
  providedIn: 'root'
})
export class ParseFile {

  constructor() {
  }

  upload(data: any, isBase64: boolean = true, file_name = 'image.jpg' ) {
    const file = isBase64 ? { base64: data } : data;
    return new Parse.File(file_name, file).save();
  }
}
