import { Injectable } from '@angular/core';
import { Preference } from './preference';

@Injectable({
  providedIn: 'root'
})
export class ExchangeRateService {

  public exhange_rate = 1
  constructor(public preference: Preference) {

  }

  public setExchangeRate(){
    this.exhange_rate = 1;
  }

  public iraqiRate(price){
    //  | currency:'د.ع'
    return `${this.formatNumber(Math.round(price * this.exhange_rate))}`;
  }

  public iraqiFormat(price){
    //  | currency:'د.ع'
    return `${this.formatNumber(Math.round(price))}`;
  }

  formatNumber(num) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
  }

  discountFormatString(price){
    console.log(price);
    let p = (price * 100).toFixed(0)
    return ` ${p}%`;
  }

  getCurrency(){
    return this.preference.lang == 'en' ? 'IQD' : 'د.ع';
  }
  
  

}
