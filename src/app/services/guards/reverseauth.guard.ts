import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { User } from '../user';

@Injectable({
  providedIn: 'root'
})
export class ReverseAuth implements CanActivate {

  constructor(private router: Router, public userService: User) {
  }

  canActivate(): Observable<boolean> | Promise<boolean> | boolean {

    return new Promise( async resolve => {
      let flag = await this.userService.getCurrent();
      if(flag){
        this.router.navigate(['1/home']);
        resolve(false);
      }else{
        resolve(true)
      }
    })
    
    // .then( )
    
    
    
    // ) {
    //   console.log("boo")
    //   this.router.navigate(['/signin']);
    //   return false;
    // }

    // return true;
  }
}