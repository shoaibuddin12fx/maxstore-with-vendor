import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router, CanActivate } from '@angular/router';
import { Observable } from 'rxjs';
import { Events } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class HomeActivateGuard implements CanActivate  {
  
  constructor(private router: Router, public events: Events) {
  }

  canActivate(): Observable<boolean> | Promise<boolean> | boolean {
    this.events.publish('slider:startautoplay')
    return true;
  }

}
