import { Injectable, Injector } from '@angular/core';
import { User } from './user';
import { ModalController, ToastController, Events, AlertController } from '@ionic/angular';
import { SignInPage } from '../pages/sign-in/sign-in';
import { Cart, CartService } from './cart';
import { TranslateService } from '@ngx-translate/core';
import { BasePage } from '../pages/base-page/base-page';

@Injectable({
  providedIn: 'root'
})
export class AddToCartService extends BasePage {
  enableMenuSwipe(): boolean {
    return true;
  }

  public isAddingToCart: boolean = false;

  constructor(injector: Injector,
    public userService: User,
    private cartService: CartService) {
    super(injector);
  }

  async presentSignInModal() {

    const modal = await this.modalCtrl.create({
      component: SignInPage,
      componentProps: {
        showLoginForm: true
      }
    });

    return await modal.present();
  }

  async onAddToCart(item) {

      this.isAddingToCart = true;
      const rawItem = Object.assign({}, item);
      let _item = await this.cartService.onAddToCart(rawItem);
      return _item;


      
  }


}
