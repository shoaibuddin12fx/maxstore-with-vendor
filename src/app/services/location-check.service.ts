import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Injectable } from '@angular/core';
import { Diagnostic } from '@ionic-native/diagnostic/ngx';
import { AlertController, Platform } from '@ionic/angular';
import { OpenNativeSettings } from '@ionic-native/open-native-settings/ngx';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';

@Injectable({
  providedIn: 'root'
})
export class LocationCheckService {

  constructor(public diagnostic: Diagnostic,
    public alertCtrl: AlertController,
    public settings: OpenNativeSettings,
    private androidPermissions: AndroidPermissions,
    private geolocation: Geolocation,
    public plt: Platform) {
      console.log('Hello LocationCheckProvider Provider');
    }

  isLocationEnabled() {

    if (this.plt.is('ios')) {
      this.isLocationEnableiOS()
    }

    if (this.plt.is('android')) {
      this.isLocationEnableAndroid()
    }

  }

  isLocationEnableiOS() {

    this.plt.ready().then( () => {
      this.diagnostic.isLocationAuthorized().then( isAuthorized => {
        console.log();
        if(isAuthorized == false){
          this.openAppSettingsPermissions();
        }

        if(isAuthorized == true){
          
          this.diagnostic.isLocationAvailable().then((state) => {

            if (state == false) {
              this.openLocationSettings()
            }

          }).catch(err => {
            console.log(err);
          })


        }

      })
    })

    

  }

  isLocationEnableAndroid() {

    this.plt.ready().then( () => {
      this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION).then(
        result => {
  
          if (result.hasPermission == false) {
            
            this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION).then(
              v => {
                if(v.hasPermission == false){
                  this.openAppSettingsPermissions();
                }
              }
            )
          }

          if (result.hasPermission == true) {

            this.diagnostic.isLocationEnabled().then( isAllowed => {
              if(isAllowed == false){
                this.openLocationSettings();
              }
            }, err => {
              console.log(err);
            })

          }

          
  
        },
        err => {
          console.log('error here');
        }
      );
    })

    

  }

  async openAppSettingsPermissions(){
    let confirm = await this.alertCtrl.create({
      backdropDismiss: false,
      header: 'Permission',
      message: 'Location permission is denied, please allow them in application settings on this device.',
      buttons: [

        {
          text: 'GO TO SETTINGS',
          handler: () => {
            this.settings.open('application_details');
          }
        }
      ]
    });
    confirm.present();
  }

  async openLocationSettings(){
    let confirm = await this.alertCtrl.create({
      backdropDismiss: false,
      header: 'Location',
      message: 'Location information is unavaliable on this device. Go to Settings to enable Location for Untapped.',
      buttons: [

        {
          text: 'GO TO SETTINGS',
          handler: () => {
            this.settings.open('location');
          }
        }
      ]
    });
    confirm.present();
  }

  getLocation(){
    return new Promise( resolve => {
      this.geolocation.getCurrentPosition().then((resp) => {
        // resp.coords.latitude
        // resp.coords.longitude
        resolve({lat: resp.coords.latitude, lng: resp.coords.longitude});
       }).catch((error) => {
         console.log('Error getting location', error);
         resolve(null);
       });
    })
  }



}
