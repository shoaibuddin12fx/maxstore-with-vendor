import { User } from "./user";
import { Events } from "@ionic/angular";
import { Router } from "@angular/router";

export class InitAppService {

  constructor(){

  }

  
  load(): Promise<any> {
    
    return new Promise( async resolve => {

      // check for direct route hit
      sessionStorage.removeItem('direct_product');
      let url = location.href.includes('1/home/items/')
      console.log(url);
      console.log(location.href);
      if(url){
        sessionStorage.setItem('direct_product', location.href);
      }

      resolve(true);
    })
    
    
  }

}
