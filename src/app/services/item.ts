import { Injectable } from '@angular/core';
import * as Parse from 'parse';

@Injectable({
  providedIn: 'root'
})
export class Item {

  constructor() {
  }

  static getInstance() {
    return this;
  }

  loadInCloud(params: any = {}){
    return [];
  }

  loadOne(id: string) {
    return [];
  }

  load(params: any = {}) {

    
    return [];
  }

  like(itemId: string) {
    return [];
  }

  isLiked(itemId: string) {
    return false;
  }

  trackView(itemId: string) {
    return [];
  }

  $key
  id;
  qty
  objectId
  name
  status
  subcategory
  images
  price
  saleprice
  isFeatured
  isNewArrival
  featuredImageThumb
  featuredImage
  description
  category
  discount
  slug
  vendor
  gallery_images
  devices
}
