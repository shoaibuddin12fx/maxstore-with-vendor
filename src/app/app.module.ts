import { NgModule, APP_INITIALIZER } from '@angular/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouteReuseStrategy, RouterModule } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AppVersion } from '@ionic-native/app-version/ngx';
import { Camera } from '@ionic-native/camera/ngx';
import { Facebook } from '@ionic-native/facebook/ngx';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { HeaderColor } from '@ionic-native/header-color/ngx';
import { Stripe } from '@ionic-native/stripe/ngx';

import { library } from '@fortawesome/fontawesome-svg-core';
import { faFacebookF } from '@fortawesome/free-brands-svg-icons/faFacebookF';
import { faTwitter } from '@fortawesome/free-brands-svg-icons/faTwitter';
import { faWhatsapp } from '@fortawesome/free-brands-svg-icons/faWhatsapp';
import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { InternationalPhoneNumberModule } from 'ngx-international-phone-number';

const icons = [faFacebookF, faTwitter, faWhatsapp];
library.add(...icons);

import 'hammerjs';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { NgxStripeModule } from 'ngx-stripe';

import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { IonicStorageModule } from '@ionic/storage';
import { ImgFallbackModule } from 'ngx-img-fallback';
import { MomentModule } from 'angular2-moment';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { SidemenuComponent } from './components/sidemenu/sidemenu.component';
import { SidemenuComponentsModule } from './components/sidemenu-component.module';
import { SignInPage } from './pages/sign-in/sign-in';
import { SignInPageModule } from './pages/sign-in/sign-in.module';
import { UniqueDeviceID } from '@ionic-native/unique-device-id/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { Diagnostic } from '@ionic-native/diagnostic/ngx';
import { OpenNativeSettings } from '@ionic-native/open-native-settings/ngx';
import { SettingsPage } from './pages/settings-page/settings-page';
import { SettingsPageModule } from './pages/settings-page/settings-page.module';
import { FCM } from '@ionic-native/fcm/ngx';
import { Toast } from '@ionic-native/toast/ngx';
import { CategoryImgSlidesComponent } from './components/category-img-slides/category-img-slides.component';
import { Firebase } from '@ionic-native/firebase/ngx';
import { FcmService } from './services/fcm.service';
import { InitAppService } from './services/init-app.service';


export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [AppComponent ],
  entryComponents: [SidemenuComponent, SignInPage, SettingsPage],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    IonicModule.forRoot({
      backButtonText: '',
      hardwareBackButton: false
    }),
    NgxStripeModule.forRoot(),
    AppRoutingModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    IonicStorageModule.forRoot(),
    ImgFallbackModule,
    MomentModule,
    SidemenuComponentsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    RouterModule,
    SignInPageModule,
    SettingsPageModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
  ],
  providers: [
    InitAppService,
    {
      provide: APP_INITIALIZER,
      useFactory: ( InitApp: InitAppService ) => () => InitApp.load(),
      deps: [InitAppService],
      multi: true
    },
    StatusBar,
    SplashScreen,
    AppVersion,
    Camera,
    FCM,
    Facebook,
    InAppBrowser,
    SocialSharing,
    HeaderColor,
    Stripe,
    UniqueDeviceID,
    Geolocation,
    AndroidPermissions,
    Diagnostic,
    Toast,
    OpenNativeSettings,
    Firebase,
    FcmService,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
