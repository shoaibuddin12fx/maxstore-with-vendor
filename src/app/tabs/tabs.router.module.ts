import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';
import { AuthGuard } from '../services/guards/auth.guard';
import { HomeActivateGuard } from '../services/guards/home-activate.guard';
import { SignInPage } from '../pages/sign-in/sign-in';
import { ReverseAuth } from '../services/guards/reverseauth.guard';
import { ItemPage } from '../pages/item/item';

const routes: Routes = [
  {
    path: '1',
    component: TabsPage,
    children: [
      {
        path: 'home',
        children: [
          {
            path: '',
            loadChildren: '../pages/home/home.module#HomePageModule',
          },
          {
            path: 'browse',
            loadChildren: '../pages/category-list/category-list.module#CategoryListPageModule'
          },
          {
            path: 'search',
            loadChildren: '../pages/search/search.module#SearchPageModule'
          },
          {
            path: 'search/:term',
            loadChildren: '../pages/search/search.module#SearchPageModule'
          },
          {
            path: 'items',
            loadChildren: '../pages/item-list/item-list.module#ItemListPageModule'
          },
          {
            path: ':categoryId/items',
            loadChildren: '../pages/item-list/item-list.module#ItemListPageModule'
          },
          {
            path: ':categoryId/items/:itemId',
            loadChildren: '../pages/item/item.module#ItemPageModule'
          },
          {
            path: 'items/:itemId',
            loadChildren: '../pages/item/item.module#ItemPageModule'
          },
          {
            path: 'items/:itemId/:slug',
            loadChildren: '../pages/item/item.module#ItemPageModule'
          },
          {
            path: ':categoryId',
            loadChildren: '../pages/category-list/category-list.module#CategoryListPageModule'
          },
          {
            path: ':categoryId/:subcategoryId',
            loadChildren: '../pages/item-list/item-list.module#ItemListPageModule'
          },
          {
            path: ':categoryId/:subcategoryId/:itemId',
            loadChildren: '../pages/item/item.module#ItemPageModule'
          },
          {
            path: ':categoryId/:subcategoryId/:itemId/:slug',
            loadChildren: '../pages/item/item.module#ItemPageModule'
          },
        ],
      },
      {
        path: 'ratings/:itemId',
        loadChildren: '../pages/ratings/ratings.module#RatingsPageModule'
      },
      {
        path: 'vendors',
        children: [
          {
            path: '',
            loadChildren: '../pages/vendor-list/vendor-list.module#VendorListPageModule'
          },
          {
            path: 'items',
            loadChildren: '../pages/item-list/item-list.module#ItemListPageModule'
          },
          {
            path: 'items/:itemId',
            loadChildren: '../pages/item/item.module#ItemPageModule'
          },
          {
            path: 'items/:itemId/:slug',
            loadChildren: '../pages/item/item.module#ItemPageModule'
          },
          {
            path: ':categoryId/items',
            loadChildren: '../pages/item-list/item-list.module#ItemListPageModule'
          },
          {
            path: ':categoryId/:subcategoryId',
            loadChildren: '../pages/item-list/item-list.module#ItemListPageModule'
          },
          {
            path: ':categoryId/:subcategoryId/:itemId',
            loadChildren: '../pages/item/item.module#ItemPageModule'
          },
          {
            path: ':categoryId/:subcategoryId/:itemId/:slug',
            loadChildren: '../pages/item/item.module#ItemPageModule'
          },
          {
            path: ':categoryId',
            loadChildren: '../pages/sub-category-list/sub-category-list.module#SubCategoryListPageModule'
          },
        ],
        
      },
      
      {
        path: 'browse',
        children: [
          {
            path: '',
            loadChildren: '../pages/category-list/category-list.module#CategoryListPageModule'
          },
          {
            path: 'items',
            loadChildren: '../pages/item-list/item-list.module#ItemListPageModule'
          },
          {
            path: 'items/:itemId',
            loadChildren: '../pages/item/item.module#ItemPageModule'
          },
          {
            path: 'items/:itemId/:slug',
            loadChildren: '../pages/item/item.module#ItemPageModule'
          },
          {
            path: ':categoryId/items',
            loadChildren: '../pages/item-list/item-list.module#ItemListPageModule'
          },
          {
            path: ':categoryId/:subcategoryId',
            loadChildren: '../pages/item-list/item-list.module#ItemListPageModule'
          },
          {
            path: ':categoryId/:subcategoryId/:itemId',
            loadChildren: '../pages/item/item.module#ItemPageModule'
          },
          {
            path: ':categoryId/:subcategoryId/:itemId/:slug',
            loadChildren: '../pages/item/item.module#ItemPageModule'
          },
          {
            path: ':categoryId',
            loadChildren: '../pages/sub-category-list/sub-category-list.module#SubCategoryListPageModule'
          },
        ],
        
      },
      {
        path: 'cart',
        children: [
          {
            path: '',
            loadChildren: '../pages/cart-page/cart-page.module#CartPageModule'
          },
          {
            path: 'items',
            loadChildren: '../pages/item-list/item-list.module#ItemListPageModule'
          },
          {
            path: 'items/:itemId',
            loadChildren: '../pages/item/item.module#ItemPageModule'
          },
          {
            path: 'checkout',
            loadChildren: '../pages/checkout-page/checkout-page.module#CheckoutPageModule'
          },
          {
            path: 'checkout/thanks/:orderId',
            loadChildren: '../pages/thanks-page/thanks-page.module#ThanksPageModule'
          }
        ],
        canActivate: [AuthGuard]
      },
      {
        path: 'account',
        children: [
          {
            path: '',
            
            loadChildren: '../pages/profile-page/profile-page.module#ProfilePageModule'
          },
          {
            path: 'items',
            loadChildren: '../pages/item-list/item-list.module#ItemListPageModule'
          },
          {
            path: 'payment',
            
            loadChildren: '../pages/card-list/card-list.module#CardListPageModule'
          },
          {
            path: 'addresses',
            
            loadChildren: '../pages/address-list/address-list.module#AddressListPageModule'
          },
          {
            path: 'help',
            
            loadChildren: '../pages/about/about.module#AboutPageModule'
          },
          {
            path: 'favorites',
            
            loadChildren: '../pages/favorite/favorite.module#FavoritePageModule'
          },
          {
            path: 'favorites/:itemId',
            
            loadChildren: '../pages/item/item.module#ItemPageModule'
          },
          {
            path: 'orders',
            
            loadChildren: '../pages/order-list-page/order-list-page.module#OrderListPageModule'
          },
          {
            path: 'orders/:id',
            
            loadChildren: '../pages/order-detail-page/order-detail-page.module#OrderDetailPageModule'
          },
          {
            path: 'orders/:id',
            
            loadChildren: '../pages/order-detail-page/order-detail-page.module#OrderDetailPageModule'
          },
          {
            path: 'notifications',
            
            loadChildren: '../pages/notification/notification.module#NotificationPageModule'
          }
        ],
        canActivate: [AuthGuard]
      },

      {
        path: 'help',
        children: [
          {
            path: '',
            loadChildren: '../pages/help/help.module#HelpPageModule'
          },
          {
            path: 'items',
            loadChildren: '../pages/item-list/item-list.module#ItemListPageModule'
          }
        ],
        canActivate: [AuthGuard]
      },

      
    ],

  },
  {
    path: '1/home/items/:itemId',
    loadChildren: '../pages/item/item.module#ItemPageModule'
  },
  {
    path: 'signin',
    component: SignInPage
  }

];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}
