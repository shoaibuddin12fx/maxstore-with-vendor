import { Component, ViewChild } from '@angular/core';
import { Platform, Events, NavController, ModalController, IonTabBar } from '@ionic/angular';
import { Preference } from '../services/preference';
import { SettingsPage } from '../pages/settings-page/settings-page';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {


  selectedIndex = 'home';
  














  constructor(public platform: Platform,
    private event: Events,
    public navCtrl: NavController,
    private modalCtrl: ModalController,
    public preference: Preference) {

      this.event.subscribe('openlink', this.openlink.bind(this));
      this.openlink('home');

    }

    @ViewChild('tabbar') tabRef: IonTabBar;

    










    openlink(link){
      if(link == 'settings'){
        this.onPresentSettingsModal() 
      }
      else{
        this.navCtrl.navigateForward('1/' + link);
      }
      
    }

    setSelectedIndex(link){
      this.selectedIndex = link;
    }

    async onPresentSettingsModal() {
    
      const modal = await this.modalCtrl.create({
        component: SettingsPage
      });
  
      return await modal.present();
    }

    











}
