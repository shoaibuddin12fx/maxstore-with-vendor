import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { ItemBlockComponent } from './item-block/item-block.component';
import { TranslateService, TranslateModule } from '@ngx-translate/core';
import { QtyCounterComponentsModule } from './qty-counter-component.module';
import { SharedModule } from '../shared.module';
import { FormsModule } from '@angular/forms';

@NgModule({
	declarations: [
		ItemBlockComponent
	],
	imports: [
		CommonModule,
		IonicModule,
		SharedModule,
		RouterModule,
		TranslateModule,
		QtyCounterComponentsModule,
		FormsModule
	],
	exports: [
		ItemBlockComponent
	]
})
export class ItemBlockComponentsModule { }
