import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { IonicModule } from "@ionic/angular";
import { RouterModule } from "@angular/router";
import { TranslateModule } from "@ngx-translate/core";
import { NgxSkeletonLoaderModule } from "ngx-skeleton-loader";
import { EmptyViewModule } from "../empty-view/empty-view.module";
import { CategoryImgSlidesComponent } from "./category-img-slides.component";
import { SharedModule } from "src/app/shared.module";




@NgModule({
	declarations: [
		CategoryImgSlidesComponent
	],
	imports: [
		CommonModule,
		IonicModule,
		RouterModule,
		TranslateModule,
		NgxSkeletonLoaderModule,
		
		EmptyViewModule,
		SharedModule,
	],
	exports: [
		CategoryImgSlidesComponent
	]
})
export class CategoryImgSlidesComponentsModule {}