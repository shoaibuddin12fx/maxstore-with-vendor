import { Component, Input, Output, EventEmitter } from "@angular/core";
import { Category } from "src/app/services/category";
import { Item } from "src/app/services/item";
import { SubCategory } from "src/app/services/sub-category";
import { trigger, transition, style, stagger, animate } from "@angular/animations";
import { query } from "@angular/core/src/render3";
import { Observable } from "rxjs";



@Component({
    selector: 'category-img-slides',
    templateUrl: 'category-img-slides.component.html',
    styleUrls: ['category-img-slides.component.scss'],
    animations: [
        trigger('staggerIn', [
          transition('* => *', [
            
            
          ])
        ])
      ]
})
export class CategoryImgSlidesComponent {
    

    _category: Category;
    _subcategory: SubCategory;
    subcategories: SubCategory[] = [];
    protected loadAndScroll: Observable<any>;

    public params: any = {
        page: 0,
        limit: 10
    };

    get category(): Category {
        return this._category;
    }

    @Input('category') set category(value: Category) {
        this._category = value;
        // console.log(value);
        this.loadCategoryData();
    }

    @Input('subcategory') subcategory: SubCategory;

    @Output('onclick') onclick: EventEmitter<any> = new EventEmitter<any>();



    selected = 'all';

    constructor(private categoryService: Category,
        private itemService: Item,
        private subCategoryService: SubCategory) { 
            
    }

    async loadCategoryData() {

        this.params.category = this.category;
        this.subcategories = await this.subCategoryService.load({category: this.category});
    
    }

    returnCategoryImage(cat){
        console.log(cat);
        const gif = 'assets/imgs/loading.gif'; 
        return cat.imageThumb ? cat.imageThumb.url ? cat.imageThumb.url : gif : gif 
    }


    
}