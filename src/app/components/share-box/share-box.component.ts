import { Component, Input, Output, EventEmitter, OnDestroy, ViewChild } from "@angular/core";
import { trigger, transition, style, stagger, animate } from "@angular/animations";
import { FirebaseService } from "src/app/services/fb/firebase.service";

@Component({
    selector: 'app-share-box',
    templateUrl: 'share-box.component.html',
    styleUrls: ['share-box.component.scss'],
    animations: [
        trigger('staggerIn', [
            transition('* => *', [


            ])
        ])
    ]
})
export class HomeBannerComponent implements OnDestroy {

    loading = true;
    placeholder = "assets/imgs/loading.gif"
    vendor: any;
    _vendor_id: any;
    _vendor_phone1: any;
    _vendor_phone2: any;
    _vendor_long: any;
    _vendor_lat: any;
    item_$key: any;
    @Input() set vendor_id(value: any){
        this._vendor_id = value;
        this.setVendor(value);
    }
    @Input() showSearchBox = true;
    @Input() set item_id(value: any){
        this.item_$key = value;
    }

    @Output('socialShare') socialShare: EventEmitter<any> = new EventEmitter<any>();

    async setVendor(id){
        this.loading = true;
        this.vendor = await this.firebaseService.getVendorListBtId(id);
        console.log(this.vendor)
        this.loading = false;

    }

    get vendor_id(): any {
        return this._vendor_id;
    }

    async setVendorPhone(phone1: any, phone2:any){
        this._vendor_phone1 = phone1;
        this._vendor_phone2 = phone2;
    }

    constructor(public firebaseService: FirebaseService) {

    }

    ngOnDestroy(): void {
        
    }

    onShare(link){
        this.socialShare.emit(link);
    }

    





}