import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { IonicModule } from "@ionic/angular";
import { RouterModule } from "@angular/router";
import { SharedModule } from "src/app/shared.module";
import { HomeBannerComponent } from "./share-box.component";





@NgModule({
	declarations: [
		HomeBannerComponent
	],
	imports: [
		CommonModule,
		IonicModule,
		RouterModule,
		SharedModule,
	],
	exports: [
		HomeBannerComponent
	]
})
export class ShareBoxComponentModule {}