import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { Events } from '@ionic/angular';
import { Preference } from 'src/app/services/preference';
import { ExchangeRateService } from 'src/app/services/exchange-rate.service';
import { Item } from 'src/app/services/item';
import { AddToCartService } from 'src/app/services/add-to-cart.service';
import { CartService } from 'src/app/services/cart';
import { IonSelect } from '@ionic/angular';
import { AlertsService } from 'src/app/services/alerts.service';
import { Router } from '@angular/router';
import { User } from 'src/app/services/user';

@Component({
  selector: 'app-item-block',
  templateUrl: './item-block.component.html',
  styleUrls: ['./item-block.component.scss']
})
export class ItemBlockComponent implements OnInit {
  @ViewChild('colorSelect') colorSelect: IonSelect;
  @ViewChild('sizeSelect') sizeSelect: IonSelect;
  @ViewChild('deviceSelect') deviceSelect: IonSelect;

  @Input() showCartIcon: boolean = true;
  @Input() itemQty: any;
  @Input() isSale: boolean = false;

  colorActionSheetOptions: any = {
    header: 'Color - اللون'
  };

  sizeActionSheetOptions: any = {
    header: 'Size - الحجم'
  };

  deviceActionSheetOptions: any = {
    header: 'Device - الحجم'
  };

  placeholder = "assets/imgs/loading.gif"
  _item: any;
  size: any;
  sizes: any;
  color: any;
  device: any;
  cartItem: any;

  @Input() set item(value: any) {
    this._item = value;
    this.setItem(value)
  }

  get item(): any {
    return this._item;
  }

  async setItem(value) {
    // this._item = value;
    // await this.itemService.loadOne(value.id);
    console.log(this._item);
    this.placeholder = value.image_firebase_url ? value.image_firebase_url : "assets/imgs/loading.gif";
    let count = await this.cartService.getCartQuantityOfProduct(this.item.id)
    this.item.qty = count;
    if (this.item["size"]) {
      this.sizes = Object.keys(this.item["size"]).map(key => {
        return this.item["size"][key];
      });
    }
  }

  addToCartActivated: boolean = false;

  constructor(private event: Events,
    private itemService: Item,
    public exrate: ExchangeRateService,
    public cartService: CartService,
    public atc: AddToCartService,
    public preference: Preference,
    public alerts: AlertsService,
    private router: Router,
    public userService: User) { }

  ngOnInit() {
    // console.log(this.item);
  }

  setColor($event, item) {
    return new Promise(async resolve => {
      $event.stopPropagation();
      let flag = await this.userService.getCurrent();
      if(!flag){
        this.router.navigate(['/signin'],  { queryParams: { showHeader: true } });
      } else {
        if (item.color) {
          this.colorSelect.open();
        } else {
          this.setSize(item);
        }
      }
    });
  }

  setSize(item) {
    return new Promise(resolve => {
      if (item.size) {
        this.sizeSelect.open();
        this.cartItem = this.item;
        console.log(this.cartItem);
      } else {
        this.setDevice(item);
      }
    });
  }

  setDevice(item) {
    return new Promise(resolve => {
      if (item.devices) {
        this.deviceSelect.open();
        this.cartItem = this.item;
      } else {
        this.addToCart(item);
      }
    });
  }

  addToCart(item: Item) {
    if (this.color && this.size) {
      item['selectedColor'] = this.color;
      item['selectedSize'] = this.size;
      item['selectedDevice'] = this.device ? this.device : "";
    }
    this.atc.onAddToCart(item).then(res => {
      console.log('adaded to cart')
      this.alerts.presentToast('Added to Cart Successfully!');
      let total = this.cartService.calculateTotal();
      let subtotal = this.cartService.calculateSubtotal();
      console.log(total, subtotal);
    });
  }

}
