import { Events, NavController } from '@ionic/angular';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Preference } from 'src/app/services/preference';
import { CanGoBackService } from 'src/app/services/can-go-back.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-black-header',
  templateUrl: './black-header.component.html',
  styleUrls: ['./black-header.component.scss']
})
export class BlackHeaderComponent implements OnInit {

  @Input() title = '';
  @Input() showLike = false;
  @Input() isLiked = false;
  @Input() isColorBg = false;
  @Input() showSearch = false;
  @Input() showLogo = false;
  @Input() isSearchOn = false;

  @Input() cangoback = false;
  @Output('onLike') onLike: EventEmitter<any> = new EventEmitter<any>();
  @Output('searchtext') searchtext: EventEmitter<any> = new EventEmitter<any>();
  
  constructor(private event: Events, 
    public canGoBack: CanGoBackService,
    public Nav: NavController,
    // private router: NavController,
    private router: Router,
    public preference: Preference) { 
      
    }

  ngOnInit() {

    console.log(this.router.url);

    if(!this.cangoback){
      const curl = this.canGoBack.getCurrentUrl();
      console.log(curl.match(/\//g).length);
      this.cangoback = (curl.match(/\//g).length > 2);
    }else{
      
    }
    
    // this.cangoback = (this.canGoBack.getCurrentUrl()) ? true : false;
  }

  openTab(link){
    this.event.publish('openlink', link);
  }

  goback(){
    window.history.back();
    // this.router.navigate([this.canGoBack.getPreviousUrl()]);
    // this.Nav.back();
    // this.Nav.pop();
  }

  SearchBy($event){
    $event.stopPropagation();
    let value = $event.target.value
    console.log(value);
    this.event.publish('product:searched', {searchString: value});
    if(!value) return 
    this.searchtext.emit(value)
  }


  
}
