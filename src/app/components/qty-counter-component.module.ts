import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { TranslateService, TranslateModule } from '@ngx-translate/core';
import { QtyCounterComponent } from './qty-counter/qty-counter.component';
import { FormsModule } from '@angular/forms';

@NgModule({
	declarations: [
		QtyCounterComponent
	],
	imports: [
		CommonModule,
		IonicModule,
		FormsModule,
		RouterModule,
		TranslateModule
		
	],
	exports: [
		QtyCounterComponent
	]
})
export class QtyCounterComponentsModule {}
