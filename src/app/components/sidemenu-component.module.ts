import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { SidemenuComponent } from './sidemenu/sidemenu.component';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
@NgModule({
	declarations: [
		SidemenuComponent
	],
	imports: [
		CommonModule,
		IonicModule,
		RouterModule,
		TranslateModule.forChild()
		
	],
	exports: [
		SidemenuComponent
	]
})
export class SidemenuComponentsModule {}
