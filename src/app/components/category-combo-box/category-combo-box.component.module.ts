import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { IonicModule } from "@ionic/angular";
import { TranslateModule } from "@ngx-translate/core";
import { SharedModule } from "src/app/shared.module";
import { CategoryComboBoxComponent } from "./category-combo-box.component";
import { ItemBlockComponentsModule } from "../item-block-component.module";




@NgModule({
	declarations: [
		CategoryComboBoxComponent
	],
	imports: [
		CommonModule,
		IonicModule,
		TranslateModule,
		SharedModule,
		ItemBlockComponentsModule
	],
	exports: [
		CategoryComboBoxComponent
	]
})
export class CategoryComboBoxComponentsModule {}