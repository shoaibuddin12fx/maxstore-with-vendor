import { Component, Input, Output, EventEmitter } from "@angular/core";
import { Category } from "src/app/services/category";
import { Item } from "src/app/services/item";
import { SubCategory } from "src/app/services/sub-category";
import { trigger, transition, style, stagger, animate } from "@angular/animations";
import { query } from "@angular/core/src/render3";
import { Observable } from "rxjs";
import { FirebaseService } from "src/app/services/fb/firebase.service";



@Component({
    selector: 'category-combo-box',
    templateUrl: 'category-combo-box.component.html',
    styleUrls: ['category-combo-box.component.scss'],
    animations: [
        trigger('staggerIn', [
          transition('* => *', [

          ])
        ])
      ]
})
export class CategoryComboBoxComponent {


    _category: any;
    _slides: any;
    items: any[] = [];
    limit = 10;

    get category(): any {
        return this._category;
    }

    get slides(): any {
        return this._slides;
    }


    @Input('category') set category(value: any) {
        this._category = value;
        console.log(value);
        this.getCategoryItems(value.id);

    }

    @Input('slides') set slides(value: any) {
        this._slides = value.filter(x => x.type === this._category.id);

        console.log(this._category.id);
        console.log(this._slides);
        console.log(value);
    }
    @Output('onclick') onclick: EventEmitter<any> = new EventEmitter<any>();
    @Output('openItem') openItem: EventEmitter<any> = new EventEmitter<any>();
    @Output('openCategory') openCategory: EventEmitter<any> = new EventEmitter<any>();


    constructor(public firebaseService: FirebaseService) { }

    async getCategoryItems(cat_id){
        
        let dataItems = await this.firebaseService.getItemListBtCategoryId(cat_id, this.limit);
        this.items = dataItems.filter(x => x.status === 'Active');
        console.log(this.items);
    }

    goToItemPage(item){
        this.openItem.emit(item);
    }

    openCategoryEvent(){
        this.openCategory.emit(this.category);
    }




}