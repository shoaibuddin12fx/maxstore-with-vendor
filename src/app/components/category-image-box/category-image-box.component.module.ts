import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { IonicModule } from "@ionic/angular";
import { TranslateModule } from "@ngx-translate/core";
import { SharedModule } from "src/app/shared.module";
import { CategoryImageBoxComponent } from "./category-image-box.component";




@NgModule({
	declarations: [
		CategoryImageBoxComponent
	],
	imports: [
		CommonModule,
		IonicModule,
		TranslateModule,
		SharedModule,
	],
	exports: [
		CategoryImageBoxComponent
	]
})
export class CategoryImageBoxComponentsModule {}