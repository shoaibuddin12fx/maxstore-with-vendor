import { Component, Input, Output, EventEmitter } from "@angular/core";
import { Category } from "src/app/services/category";
import { Item } from "src/app/services/item";
import { SubCategory } from "src/app/services/sub-category";
import { trigger, transition, style, stagger, animate } from "@angular/animations";
import { query } from "@angular/core/src/render3";
import { Observable } from "rxjs";



@Component({
    selector: 'category-image-box',
    templateUrl: 'category-image-box.component.html',
    styleUrls: ['category-image-box.component.scss'],
    animations: [
        trigger('staggerIn', [
          transition('* => *', [
            
            
          ])
        ])
      ]
})
export class CategoryImageBoxComponent {
    

    _category: any;

    get category(): any {
        return this._category;
    }

    @Input('category') set category(value: any) {
        this._category = value;
    }

    @Output('onclick') onclick: EventEmitter<any> = new EventEmitter<any>();


    constructor() { 
            
    }

    returnCategoryImage(cat){
        console.log(cat);
        const gif = 'assets/imgs/loading.gif'; 
        return cat.imageThumb ? cat.imageThumb.url ? cat.imageThumb.url : gif : gif 
    }


    
}