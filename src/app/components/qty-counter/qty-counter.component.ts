import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-qty-counter',
  templateUrl: './qty-counter.component.html',
  styleUrls: ['./qty-counter.component.scss']
})
export class QtyCounterComponent implements OnInit {


  @Input() item: any;
  @Input() qty: number = 1;
  @Output('qtyChange') qtyChange: EventEmitter<any> = new EventEmitter<any>();
  constructor() { }

  ngOnInit() {

  }

  incrementQuantity(item){
    this.qty++;
    this.qtyChange.emit(this.qty);
  }

  decrementQuantity(item){
    this.qty--;
    if(this.qty < 1){
      this.qty = 1;
    }
    this.qtyChange.emit(this.qty);
  }

}
