import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { BlackHeaderComponent } from './black-header/black-header.component';
@NgModule({
	declarations: [
		BlackHeaderComponent
	],
	imports: [
		CommonModule,
		IonicModule,
		RouterModule,
		
	],
	exports: [
		BlackHeaderComponent
	]
})
export class BlackHeaderComponentsModule {}
