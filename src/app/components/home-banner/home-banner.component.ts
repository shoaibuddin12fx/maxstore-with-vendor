import { Component, Input, Output, EventEmitter, OnDestroy, ViewChild } from "@angular/core";
import { Category } from "src/app/services/category";
import { Item } from "src/app/services/item";
import { SubCategory } from "src/app/services/sub-category";
import { trigger, transition, style, stagger, animate } from "@angular/animations";
import { query } from "@angular/core/src/render3";
import { Observable } from "rxjs";
import { IonSlides } from "@ionic/angular";



@Component({
    selector: 'home-banner',
    templateUrl: 'home-banner.component.html',
    styleUrls: ['home-banner.component.scss'],
    animations: [
        trigger('staggerIn', [
            transition('* => *', [


            ])
        ])
    ]
})
export class HomeBannerComponent implements OnDestroy {

    public slidesConfig = {
        slidesPerView: 1,
        spaceBetween: 7,
        grabCursor: true,
        zoom: false,
        autoplayDisableOnInteraction: false,
        loop: true,
        speed: 500,
        breakpointsInverse: false,
        autoplay: {
            delay: 2000,
        },

        breakpoints: {
            992: {
                slidesPerView: 1,
                spaceBetween: 20,
                loop: false,
            },
        }
    };


    timer;
    gif = 'assets/imgs/loading.gif';
    _loadurl: string = this.gif;
    randomNumber = 0;

    get loadurl(): string {
        return this._loadurl;
    }

    @Input('loadurl') set loadurl(value: string) {
        this._loadurl = value;
        this.refreshImageRepeatedly(value);
    }

    @Input('heading') heading: string;
    @Input('subheading') subheading: string;
    @Input('btnText') btnText: string;
    @ViewChild(IonSlides) ionSlides: IonSlides;
    @Input('slides') slides: any[];

    

    constructor() {

    }

    ngOnDestroy(): void {
        clearInterval(this.timer);
    }

    refreshImageRepeatedly(img) {

        this.timer = setInterval(() => {

            let random = Math.random();
            let m = `${img}?random=${random}`;
            console.log(m);
            this.removeParam('random', this._loadurl)
            this._loadurl = m;


        }, 30000)



    }

    removeParam(key, sourceURL) {
        var rtn = sourceURL.split("?")[0],
            param,
            params_arr = [],
            queryString = (sourceURL.indexOf("?") !== -1) ? sourceURL.split("?")[1] : "";
        if (queryString !== "") {
            params_arr = queryString.split("&");
            for (var i = params_arr.length - 1; i >= 0; i -= 1) {
                param = params_arr[i].split("=")[0];
                if (param === key) {
                    params_arr.splice(i, 1);
                }
            }
            rtn = rtn + "?" + params_arr.join("&");
        }
        return rtn;
    }

    onSlidesDidLoad() {
        this.fireAutoPlay()
    }

    ionSlideTouchEnd($event){

    }

    fireAutoPlay(){
        if(this.ionSlides){
            this.ionSlides.startAutoplay();
        }
    }







}