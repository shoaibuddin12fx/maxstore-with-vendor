import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { ItemBlockComponentsModule } from 'src/app/components/item-block-component.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { CategorySlidesComponent } from './category-slides.component';
import { TranslateModule } from '@ngx-translate/core';
import { EmptyViewModule } from '../empty-view/empty-view.module';

@NgModule({
	declarations: [
		CategorySlidesComponent
	],
	imports: [
		CommonModule,
		IonicModule,
		RouterModule,
		TranslateModule,
		ItemBlockComponentsModule,
		NgxSkeletonLoaderModule,
		EmptyViewModule
	],
	exports: [
		CategorySlidesComponent
	]
})
export class CategorySlidesComponentsModule {}
