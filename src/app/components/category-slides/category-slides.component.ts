import { Component, Input, EventEmitter, Output, ViewChild } from '@angular/core';
import { Category } from 'src/app/services/category';
import { SubCategory } from 'src/app/services/sub-category';
import { Item } from 'src/app/services/item';
import { IonSegment } from '@ionic/angular';

@Component({
  selector: 'category-slides',
  templateUrl: 'category-slides.component.html',
  styleUrls: ['category-slides.component.scss']
})
export class CategorySlidesComponent {

  @ViewChild('ionsagment') ionsagment: IonSegment; 

  _category: Category;
  public subcategories: SubCategory[] = [];  
  public items: Item[] = [];
  public params: any = {
    page: 0,
    limit: 10
  };
  selected = 'all';

  _obj = [];


  get category(): Category {
    return this._category;
  }

  @Input('category') set category(value: Category) {
    this._category = value;
    // console.log(value);
    this.loadCategoryData();
  }

  @Output('removeEmpty') removeEmpty: EventEmitter<any> = new EventEmitter<any>();
  @Output('itemClick') itemClick: EventEmitter<any> = new EventEmitter<any>();

  constructor(private categoryService: Category,
    private itemService: Item,
    private subCategoryService: SubCategory) { }

  segmentChanged($event) {
    let val = $event.target.value;
    this.selected = val;
    // console.log(val)
    // if (val == 'all') {
    //   this.loadCategoryData();
    // } else {
    //   const subcat: SubCategory = this.subcategories.filter(x => x.name == val)[0];
    //   this.loadSubcategoryData(subcat);
    // }
  }

  async loadCategoryData() {

    this.items = [];
    this.params.category = this.category;
    console.log(this.category);

    this.items = await this.itemService.load(this.params);
    if(this.items.length == 0){
      this.removeEmpty.emit(this.category);
    }

    this.subcategories = await this.subCategoryService.load({category: this.category});
    console.log(this.subcategories);
    this.loadAllCategories().then( ( ind: any[] ) => {
      console.log(ind);
      this.subcategories = this.subcategories.filter( x => ind.includes(x['id']) );
    })

  }

  loadAllCategories(){
    

    return new Promise( async resolve => {
      var indices = [];
      for(var i = 0; i < this.subcategories.length; i++){
        let isEmpty = await this.loadSubcategoryData(this.subcategories[i]);
        if(!isEmpty){
          indices.push(this.subcategories[i]['id']);
        }
      }
      resolve(indices);
    })
  }

  removesub(){

  }

  async loadSubcategoryData(subcat: SubCategory) {

    return new Promise( async resolve => {

      // console.log(subcat);
      let items = [];
  
      try {
        this.params.subcategory = subcat;
        items = await this.itemService.load(this.params);
        // console.log(items);
  
        if(items.length == 0){
          //subcat['items'] = []
          resolve(true);
        }else{
          // console.log(items);
          subcat['items'] = items;
          resolve(false);
        }

      } catch (error) {
        resolve(true);
        //this.translate.get('ERROR_NETWORK').subscribe((str) => this.showToast(str));
      }

    })
    

  }

}
