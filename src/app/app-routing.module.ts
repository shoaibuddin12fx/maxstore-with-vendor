import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', loadChildren: './tabs/tabs.module#TabsPageModule' },

  // {
  //   path: 'sign-in',
  //   loadChildren: './pages/sign-in/sign-in.module#SignInPageModule'
  // }
  // { path: 'menu', loadChildren: './pages/menu/menu.module#MenuPageModule' },
  // { path: 'petsell', loadChildren: './pages/petsell/petsell.module#PetsellPageModule' },
  // { path: 'notification', loadChildren: './pages/notification/notification.module#NotificationPageModule' },
  // { path: 'vendor-list', loadChildren: './pages/vendor-list/vendor-list.module#VendorListPageModule' }
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, {  onSameUrlNavigation: 'reload', preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
