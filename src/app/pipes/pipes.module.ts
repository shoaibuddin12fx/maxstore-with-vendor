import { NgModule } from '@angular/core';
import { Filter } from './filter';
import { ExcerptFilter } from './excerpt-filter';
import { ProductFilterPipe } from './product-filter.pipe';
@NgModule({
	declarations: [
		Filter,
		ExcerptFilter,
		ProductFilterPipe
	],
	imports: [],
	exports: [
		Filter,
		ExcerptFilter,
		ProductFilterPipe
	]
})
export class PipesModule { }
