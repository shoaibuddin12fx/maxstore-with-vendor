import { Component, Injector, ViewChild } from '@angular/core';
import { BasePage } from '../base-page/base-page';
import { User } from '../../services/user';
import { ProfileEditPage } from '../profile-edit/profile-edit';
import { ChangePasswordPage } from '../change-password/change-password';
import { SettingsPage } from '../settings-page/settings-page';
import { SignInPage } from '../sign-in/sign-in';
import { LocationCheckService } from 'src/app/services/location-check.service';
import { ExchangeRateService } from './../../services/exchange-rate.service';
import { IonSlides, IonContent } from '@ionic/angular';
import { Slide } from '../../services/slide';
import { Item } from '../../services/item';
import * as Parse from 'parse';
import { Category } from '../../services/category';
import { SubCategory } from '../../services/sub-category';
import { Subject, Observable, merge } from 'rxjs';
import {
  trigger,
  style,
  animate,
  transition,
  query,
  stagger,
} from '@angular/animations';
import { AddToCartService } from 'src/app/services/add-to-cart.service';
import { UniqueDeviceID } from '@ionic-native/unique-device-id/ngx';
import { environment } from 'src/environments/environment';
import { Banner } from 'src/app/services/banner';
import { FirebaseService } from 'src/app/services/fb/firebase.service';
import { FbUserService } from 'src/app/services/fb/fb-user.service';
@Component({
  selector: 'page-profile-page',
  templateUrl: 'profile-page.html',
  styleUrls: ['profile-page.scss'],
  animations: [
    trigger('staggerIn', [
      transition('* => *', [
        query(':enter', style({ opacity: 0, transform: `translate3d(0,10px,0)` }), { optional: true }),
        query(':enter', stagger('70ms', [animate('100ms', style({ opacity: 1, transform: `translate3d(0,0,0)` }))]), { optional: true })
      ])
    ])
  ]
})
export class ProfilePage extends BasePage {

  public user: any;
  private loginSubscription: any;

  constructor(
    injector: Injector,
    public firebaseService: FirebaseService,
    public userService: FbUserService
  ) {
    super(injector);
  }
  enableMenuSwipe() {
    return false;
  }
  ngOnInit(): void {
    this.loginSubscription = (user) => {
      this.loginHandler(user);
    };
    this.events.subscribe('user:login', this.loginSubscription);
  }

  async ionViewDidEnter() {
    if (!this.firebaseService.isAuthenticated()) {
      await this.removeAuth();
      await this.firebaseLogin()
    } else {
      this.getCurrentUser();
      return;
    }
  }

  async removeAuth() {
    return this.firebaseService.logout();
  }

  firebaseLogin() {

    console.log("pull")
    return new Promise(async resolve => {

      const modal = await this.modalCtrl.create({
        component: SignInPage,
      });

      modal.present();

      const { data } = await modal.onWillDismiss();

      if (data) {
        console.log(data);
        resolve(data);
      }
    })
  }
  loginHandler(user: User) {
    this.user = user;
  }

  goTo(page: string) {
    this.router.navigate([this.currentPath + '/' + page], {queryParams: { cangoback: true}})
  }

  async onPresentSignInModal() {
    const modal = await this.modalCtrl.create({
      component: SignInPage,
      componentProps: {
        showLoginForm: true
      }
    });

    return await modal.present();
  }

  async onPresentSignUpModal() {
    const modal = await this.modalCtrl.create({
      component: SignInPage,
      componentProps: {
        showSignUpForm: true
      }
    });

    return await modal.present();
  }

  async onPresentEditModal() {

    const modal = await this.modalCtrl.create({
      component: ProfileEditPage
    });

    return await modal.present();
  }

  async onPresentChangePasswordModal() {

    const modal = await this.modalCtrl.create({
      component: ChangePasswordPage
    });

    return await modal.present();
  }

  async onPresentSettingsModal() {

    const modal = await this.modalCtrl.create({
      component: SettingsPage
    });

    return await modal.present();
  }

  onLogout() {
    this.events.publish('user:logout');
  }

  onSearch(value) {
    this.navigateTo(this.currentPath + '/items');
  }

  async getCurrentUser() {
    this.user = await this.userService.getAuthUser();
    console.log(this.user)
  }
}
