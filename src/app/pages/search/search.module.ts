import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SearchPage } from './search';
import { SharedModule } from '../../shared.module';
import { ItemBlockComponentsModule } from 'src/app/components/item-block-component.module';

@NgModule({
  declarations: [
    SearchPage,
  ],
  imports: [
    RouterModule.forChild([
      {
        path: '',
        component: SearchPage
      }
    ]),
    SharedModule,
    ItemBlockComponentsModule
  ],
})
export class SearchPageModule {}
