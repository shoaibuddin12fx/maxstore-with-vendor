import { CategoryImageBoxComponentsModule } from 'src/app/components/category-image-box/category-image-box.component.module';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SubCategoryListPage } from './sub-category-list';
import { SharedModule } from '../../shared.module';

@NgModule({
  declarations: [
    SubCategoryListPage,
  ],
  imports: [
    RouterModule.forChild([
      {
        path: '',
        component: SubCategoryListPage
      }
    ]),
    CategoryImageBoxComponentsModule,
    SharedModule
  ],
})
export class SubCategoryListPageModule { }
