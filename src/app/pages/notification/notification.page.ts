import { Component, OnInit } from '@angular/core';
import { Notification } from 'src/app/services/notifications';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.page.html',
  styleUrls: ['./notification.page.scss'],
})
export class NotificationPage implements OnInit {

  public notifications: any[] = []
  constructor(public notificationService: Notification) { }

  ngOnInit() {
    this.initialize()
  }

  async initialize(){
    this.notifications = await this.notificationService.load();
  }

}
