import { Component, Injector, ViewChild, OnInit } from '@angular/core';
import { BasePage } from '../base-page/base-page';
import { AppVersion } from '@ionic-native/app-version/ngx';
import { IonContent } from '@ionic/angular';
import { Subject, Observable, merge } from 'rxjs';
import { FirebaseService } from 'src/app/services/fb/firebase.service';

@Component({
  selector: 'page-about',
  templateUrl: 'about.html',
  styleUrls: ['about.scss']
})
export class AboutPage extends BasePage implements OnInit {
  @ViewChild(IonContent) container: IonContent;

  public version: string;
  public about: any;
  protected contentLoaded: Subject<any>;
  protected loadAndScroll: Observable<any>;
  public item: any;
  _title;

  constructor(injector: Injector,
    public firebaseService: FirebaseService,
    public appVersion: AppVersion
) {
    super(injector);
  }

  enableMenuSwipe() {
    return false;
  }

  ngOnInit() {
    this.setupObservable();
  }

  async ionViewDidEnter() {
    this.showLoadingView({ showOverlay: false });
    this.loadData();
    this.loadAppVersion();

    const title = await this.getTrans('SUPPORT');
    this._title = title;
    this.setPageTitle(title);

    this.setMetaTags({
      title: title
    });
  }

  async loadAppVersion() {
    try {
      this.version = await this.appVersion.getVersionNumber();
    } catch (error) {
      console.log(error);
    }
  }

  setupObservable() {
    this.loadAndScroll = merge(
      this.container.ionScroll,
      this.contentLoaded
    );
  }

  async loadData(event: any = {}) {

    this.refresher = event.target;

    try {

      this.about = await this.firebaseService.getAppDetails();
      console.warn(this.about);
      // this.about = this.item;
      // console.log(this.item);

      this.onRefreshComplete();
      this.showContentView();
      this.onContentLoaded();

    } catch (error) {
      this.translate.get('ERROR_NETWORK').subscribe((str) => this.showToast(str));
      this.onRefreshComplete();
      this.showContentView();
    }

  }

  onContentLoaded() {
    setTimeout(() => {
      this.contentLoaded.next();
    }, 400);
  }

  onSearch(value) {
    this.navigateTo(this.currentPath + '/items');
  }
}
