import { ExchangeRateService } from './../../services/exchange-rate.service';
import { Component, Injector } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { BasePage } from '../base-page/base-page';
import { Cart, CartService } from '../../services/cart';
import { Order } from '../../services/order';
import { User } from '../../services/user';
import { CustomerAddress } from '../../services/customer-address';
import { Card } from '../../services/card';
import { AddressListModalPage } from '../address-list-modal/address-list';
import { CardListModalPage } from '../card-list-modal/card-list';
import { LocationCheckService } from 'src/app/services/location-check.service';
import { Zone } from 'src/app/services/zone';

@Component({
  selector: 'page-checkout-page',
  templateUrl: 'checkout-page.html',
  styleUrls: ['checkout-page.scss']
})
export class CheckoutPage extends BasePage {

  public user: any;
  public cart: Cart;
  public form: FormGroup;
  public shippingfee = 0;
  public address: CustomerAddress;
  public card: Card;
  public zones: any[] = [];
  public subzones: any[] = [];
  public selected_subzones: any[] = [];
  public isLoadingCards: boolean;
  public isCreatingOrder: boolean;

  constructor(injector: Injector,
    public userService: User,
    private cardService: Card,
    private cartService: CartService,
    private locationChk: LocationCheckService,
    public exrate: ExchangeRateService,
    private zoneService: Zone,
    private customerAddressService: CustomerAddress) {
    super(injector);
  }

  ngOnInit() {

    this.loadZones();
    this.setupForm();
    // this.locationChk.isLocationEnabled();

  }

  enableMenuSwipe(): boolean {
    return false;
  }

  setupForm() {
    this.form = new FormGroup({
      zone: new FormControl(null, Validators.required),
      subzone: new FormControl(null, Validators.required),
      address: new FormControl(''),
      name: new FormControl('', Validators.required),
      shipping: new FormControl(null),
      card: new FormControl(null),
      paymentMethod: new FormControl('Cash'),
      contactNumber: new FormControl('', Validators.required),
      email: new FormControl(''),
      status: new FormControl('Pending'),


    });

    this.user = this.firebaseService.isAuth().currentUser;
    console.log(this.user);
  }

  async ionViewDidEnter() {

    if (this.firebaseService.isAuthenticated()) {
      this.showLoadingView({ showOverlay: false });
      this.loadCart();
    } else {
      this.showEmptyView();
    }

    const title = await this.getTrans('CHECKOUT');
    this.setPageTitle(title);

    this.setMetaTags({
      title: title
    });
  }

  async loadCart() {

    try {
      
      this.cart = await this.cartService.get();

      if (this.cart && this.cart.items.length != 0) {
        
        this.loadAddresses();
        this.cartService.calculateTotal();
      } else {
        this.showEmptyView();
      }



    } catch (error) {
      this.showContentView();
      this.translate.get('ERROR_NETWORK').subscribe(str => this.showToast(str));
    }
  }

  async loadAddresses() {

    try {

      const addresses = await this.customerAddressService.load();

      if (addresses.length) {

        this.address = addresses[0];
        console.log(addresses[0]);
        // this.cart.shipping = this.address;
        this.form.controls.shipping.setValue(this.address);
        this.cartService.calculateSubtotal();
        this.cartService.calculateTotal();
      }

      this.showContentView();

    } catch (error) {
      this.showErrorView();
      this.translate.get('ERROR_NETWORK').subscribe((str) => this.showToast(str));
    }

  }

  async loadCards() {

    try {

      this.isLoadingCards = true;

      const cards = await this.cardService.load();

      if (cards.length) {
        this.card = cards[0];
        this.form.controls.card.setValue(this.card);
      } else {
        this.onChangeCard();
      }

      this.isLoadingCards = false;

    } catch (error) {
      this.translate.get('ERROR_NETWORK').subscribe((str) => this.showToast(str));
      this.isLoadingCards = false;
    }

  }

  async onChangeAddress() {

    const modal = await this.modalCtrl.create({
      component: AddressListModalPage
    });

    modal.present();

    const { data } = await modal.onWillDismiss();

    if (data) {
      this.address = data;
      // this.cart.shipping = this.address;
      this.form.controls.shipping.setValue(this.address);
      this.cartService.calculateTotal();
    }
  }

  async onChangeCard() {

    const modal = await this.modalCtrl.create({
      component: CardListModalPage
    });

    modal.present();

    const { data } = await  modal.onWillDismiss();

    if (data) {
      this.card = data;
      this.form.controls.card.setValue(data);
    } else if (!data && !this.card) {
      this.form.controls.paymentMethod.setValue('Cash');
      this.form.controls.card.setValue(null);
    }
  }

  onChangePaymentMethod(event: any = {}) {

    const paymentMethod = event.target.value;

    if (paymentMethod === 'Cash') {
      this.form.controls.card.clearValidators();
      this.form.controls.card.setValue(null);
      this.form.controls.card.updateValueAndValidity();
    } else if (paymentMethod === 'Card') {
      this.form.controls.card.setValidators(Validators.required);
      this.form.controls.card.updateValueAndValidity();
      this.form.controls.card.setValue(this.card);

      if (!this.card) this.loadCards();
    } else {
      // no match
    }
  }

  prepareOrderData(order) {

    const formData = Object.assign({}, this.form.value);
    order.items = this.cart.items as any[];

    let order_vendors = [];
    for(var i = 0; i < order.items.length; i++ ){
        let vendor = order.items[i]["vendor"];

        let findIndex = order_vendors.findIndex( x => x.vendor_id == vendor);
        if(findIndex == -1){
          let obj = {
            vendor_id: order.items[i]['vendor'],
            status: "Pending"
          }
          order_vendors.push(obj);

        }
    }

    order.vendors = order_vendors;


    this.cartService.calculateTotal();
    order.items = this.cart.items;
    order.total = this.cart.total;
    order.paymentMethod = formData.paymentMethod;
    order.card = formData.card;
    // order.shipping = formData.shipping;
    order.shipping = this.cart.shipping;
    order.contactNumber = formData.contactNumber;
    order.email = formData.email;
    order.city = formData.zone;
    order.district = formData.subzone;

    return order;
  }
  // async emptycart(){
  //   await this.cart.items.forEach(()=>{
  //     this.cart.items.pop();
  //   })
  // }
  async onPlaceOrder() {

      if (this.form.invalid) {
        return this.translate.get('INVALID_FORM').subscribe(str => this.showToast(str));
      }

      this.isCreatingOrder = true;
      var tempVar = this.form.value;
      tempVar['profile_name'] = tempVar['name'];
      // var add = await this.customerAddressService.create({zone: tempVar['zone'], subzone: tempVar['subzone'], address: tempVar['address'] });

      // // merge two objects
      const object = {};
      console.log(object);
      var order = await this.prepareOrderData(object);
      
      order['location'] = await this.locationChk.getLocation();
      order = {...order, ...tempVar}
      console.log(order);
      let placedorder = await this.firebaseService.addOrders(order);
      console.log(placedorder);
      this.isCreatingOrder = false;
      //this.emptycart();
      this.events.publish('cart:updated', new Cart);
      const path = this.currentPath + '/thanks/' + placedorder;

      this.router.navigate([path], { replaceUrl: true });


  }

  // new fields
  async loadZones() {
      this.zones = await this.firebaseService.getCityName();
      this.subzones = await this.firebaseService.getCityDistrictName();
  }

  async onZoneChanged() {
    

      let zone_id = this.form.controls.zone.value;
      let shippingfee = this.zones.find(x => x.id == zone_id).shippingfee

      console.log(shippingfee);
      console.log(zone_id);

      this.form.controls.subzone.setValue(null);
      this.form.controls.shipping.setValue(shippingfee);
      this.cartService.setShippingFee(shippingfee);
      this.selected_subzones = this.subzones.filter( x => x.city == zone_id );
      // console.log(this.subzones);

  }

  async onSubZoneChanged(subzone){
    let sz: Zone = this.form.controls.subzone.value;
    this.shippingfee = sz.fee;
  }
}
