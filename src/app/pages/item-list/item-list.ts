import { Component, Injector } from '@angular/core';
import { IonRefresher, IonInfiniteScroll } from '@ionic/angular';
import { Item } from '../../services/item';
import { BasePage } from '../base-page/base-page';
import { SubCategory } from '../../services/sub-category';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { SharePage } from '../share/share.page';
import { Subject, Observable, merge } from 'rxjs';
import { Category } from '../../services/category';
import {
  trigger,
  style,
  animate,
  transition,
  query,
  stagger
} from '@angular/animations';
import { AddToCartService } from 'src/app/services/add-to-cart.service';
import { environment } from 'src/environments/environment';
import { Banner } from 'src/app/services/banner';

@Component({
  selector: 'page-item-list',
  templateUrl: 'item-list.html',
  styleUrls: ['item-list.scss'],
  animations: [
    trigger('staggerIn', [
      transition('* => *', [
        query(':enter', style({ opacity: 0, transform: `translate3d(0,10px,0)` }), { optional: true }),
        query(':enter', stagger('100ms', [animate('300ms', style({ opacity: 1, transform: `translate3d(0,0,0)` }))]), { optional: true })
      ])
    ])
  ]
})
export class ItemListPage extends BasePage {

  public _title = '';
  public items: Item[] = [];
  public skeletonArray = Array(12);
  public itemId: any;
  public showSearch: boolean = false;
  public params: any = {
    page: 0,
    limit: 40
  };

  public category: Category;
  public subcategory: SubCategory;
  public searchText: string;
  public item: Item;
  public vendorId;
  public categories = [];
  public webSocialShare: { show: boolean, share: any, onClosed: any } = {
    show: false,
    share: {
      config: [{
        facebook: {
          socialShareUrl: '',
        },
      }, {
        twitter: {
          socialShareUrl: '',
        }
      }, {
        whatsapp: {
          socialShareText: '',
          socialShareUrl: '',
        }
      }]
    },
    onClosed: () => {
      this.webSocialShare.show = false;
    }
  };


  constructor(injector: Injector,
    public atc: AddToCartService,
    private socialSharing: SocialSharing,
    private categoryService: Category,
    private subCategoryService: SubCategory,
    public bannerService: Banner,
    private itemService: Item) {
    super(injector);
    this.events.subscribe('product:searched', (value) => {
      console.log(value);
      this.searchText = value.searchString;
    });
  }

  public banners: any[] = []
  public sortOptions: Array<any>;
  public selectedSortOption: any;

  ngOnInit() {

    console.log(this.getQueryParams())
    this.searchText = this.getQueryParams().search;
    this.params.sale = this.getQueryParams().sale;
    this.params.new = this.getQueryParams().new;
    this.params.featured = this.getQueryParams().featured;
    this._title = this.getQueryParams().title;




    // this.loadBanners()
    // this.buildSortOptions()


  }




  buildSortOptions() {

    this.sortOptions = [{
      sortBy: 'desc', sortByField: 'createdAt',
    }, {
      sortBy: 'asc', sortByField: 'netPrice',
    }, {
      sortBy: 'desc', sortByField: 'netPrice',
    }, {
      sortBy: 'desc', sortByField: 'ratingTotal',
    }]

    const sortBy = this.getQueryParams().sortBy;
    const sortByField = this.getQueryParams().sortByField;

    if (sortBy && sortByField) {
      this.selectedSortOption = { sortBy, sortByField };
    } else {
      this.selectedSortOption = this.sortOptions[0];
    }
  }

  onSortOptionTouched(event: any = {}) {

    const option = Object.assign({}, event.detail.value);
    delete option.id;

    this.params = {
      ...this.params,
      ...option
    };

    this.onRefresh();

    this.navigateToRelative('.', option);
  }

  compareSortOption(o1: any, o2: any) {
    return o1 && o2 ? (o1.sortBy === o2.sortBy && o1.sortByField === o2.sortByField) : o1 === o2;
  };

  async loadBanners() {

    return new Promise(async resolve => {

      var params = {};
      if (this.params.new) {
        params['type'] = 'New Arrival';
        this.banners = await this.bannerService.load(params);
      }

      if (this.params.sale) {
        params['type'] = 'On sale';
        this.banners = await this.bannerService.load(params);
      }

      if (this.params.featured) {
        params['type'] = 'Featured';
        this.banners = await this.bannerService.load(params);
      }

      console.log(this.banners);
      resolve();

    })

  }

  async setCategoryTitle(id) {
    // const category = new Category;
    // category.id = id
    return new Promise(async resolve => {
      this.category = await this.firebaseService.getCategoryListBtId(id);
      this._title = this.category['name'];
      resolve()
    })


  }

  async setSubCategoryTitle(id) {
    const subcategory = new SubCategory;
    subcategory.id = id
    const subcategories = await this.subCategoryService.load(subcategory);
    console.log(subcategories);

    this._title = subcategories.filter(x => x.id == id)[0]['name'];
  }

  enableMenuSwipe(): boolean {
    return false;
  }

  async ionViewDidEnter() {

    if (!this.items.length) {
      this.showLoadingView({ showOverlay: false });
      this.loadData();
    }

    const title = await this.getTrans('ITEMS');
    this.setPageTitle(title);

    this.setMetaTags({
      title: title
    });

  }

  onRefresh(event: any = {}) {
    this.refresher = event.target;
    this.items = [];
    this.params.page = 0;
    this.loadData();
  }

  async loadData() {

    try {

      const categoryId = this.getQueryParams().catId ? this.getQueryParams().catId : this.getParams().categoryId;
      const subcategoryId = (this.getQueryParams().subCatId) ? this.getQueryParams().subCatId : this.getParams().subcategoryId;

      this.vendorId = this.getQueryParams().vendorId;
      this.categories = await this.firebaseService.getCategoryList();
      console.log(categoryId);
      if (categoryId) {
        // const category = new Category;
        // category.id = categoryId
        // this.params.category = category;
        this.setCategoryTitle(categoryId)

        const sid = subcategoryId != null ? subcategoryId : categoryId;


        if (subcategoryId != null) {
          let dataItems = await this.firebaseService.getItemListBySubCategoryId(sid);
          this.items = dataItems.filter(x => x.status === 'Active');
        } else {
          let dataItems = await this.firebaseService.getItemListBtCategoryId(sid);
          this.items = dataItems.filter(x => x.status === 'Active');
        }
      } else if (subcategoryId) {
        let dataItems = await this.firebaseService.getItemListBySubCategoryId(subcategoryId);
        this.items = dataItems.filter(x => x.status === 'Active');

      } else if (this.params.featured) {
        let dataItems = await this.firebaseService.getfeturedList();
        this.items = dataItems.filter(x => x.status === 'Active');

      } else if (this.vendorId) {
        // console.log(vendorId);
        let dataItems = await this.firebaseService.getItemListByVendorID(this.vendorId);
        this.items = dataItems.filter(x => x.status === 'Active');

      } else {
        this.items = await this.firebaseService.getItemList();
      }


      if (this.items.length) {
        this.showContentView();
      } else {
        this.showEmptyView();
      }

      this.onRefreshComplete();

    } catch (error) {
      this.showContentView();
      this.onRefreshComplete();
      this.translate.get('ERROR_NETWORK').subscribe((str) => this.showToast(str));
    }

  }

  onSearchToggle($event) {
    console.log("boo")
    this.showSearch = !this.showSearch;
  }

  onSearch($event) {

    const val: string = $event;
    const canonical = (val && val.trim() != '') ? val.toLowerCase() : null;

    this.params.canonical = canonical;
    this.params.page = 0;

    this.items = [];

    this.showLoadingView({ showOverlay: false });
    this.loadData();
  }

  onSearchCleared() {
    this.params.canonical = '';
    this.params.page = 0;
    this.items = [];
    this.showLoadingView({ showOverlay: false });
    this.loadData();
  }

  goToItemPage(item: Item) {
    this.navigateTo(this.currentPath + '/' + item.id);
  }

  onLoadMore(event: any = {}) {
    this.infiniteScroll = event.target;
    this.params.page++;
    this.loadData();
  }

  addToCart($event, item: Item) {
    console.log("prevent?");
    $event.stopPropagation();
    this.atc.onAddToCart(item);
  }

  onclick(value) {
    if (!value) return;
    console.log(value);
    this.items = [];
    if (value == 'all') {
      this.params.subcategory = null;
      this.loadData();
    } else {
      const subcategory = new SubCategory;
      subcategory.id = value
      this.params.subcategory = subcategory;
      this.setSubCategoryTitle(value);
      this.loadData();
    }
  }

  async onShare() {

    if (this.isHybrid()) {

      try {
        const url = this.getShareUrl(this.item.slug);
        await this.socialSharing.share(this.item.name, null, null, url);
      } catch (err) {
        console.warn(err);
      }

    } else if (this.isPwa() || this.isMobile()) {
      this.webSocialShare.show = true;
    } else {
      this.openShareModal();
    }

  }

  async openShareModal() {
    const modal = await this.modalCtrl.create({
      component: SharePage,
      componentProps: {
        url: this.getShareUrl(this.item.slug)
      }
    });
    return await modal.present();
  }

  async onCategoryTouched($event) {
    this.items = await this.getCategoryProducts($event);
  }
  getCategoryProducts(id) {
    return new Promise<any>(resolve => {
      if (id) {
        let products = this.items;
        let filteredProducts = [];
        products.forEach(item => {
          if (item.category || item.subcategory) {
            if (item.category == id || item.subcategory == id) {
              filteredProducts.push(item);
              resolve(filteredProducts);
            }
          } else {
            resolve(products)
          }
        })
      }
    })
  }

}
