import { CategoryImgSlidesComponent } from './../../components/category-img-slides/category-img-slides.component';
import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ItemListPage } from './item-list';
import { SharedModule } from '../../shared.module';
import { ItemBlockComponentsModule } from 'src/app/components/item-block-component.module';
import { CategoryImgSlidesComponentsModule } from 'src/app/components/category-img-slides/category-img-slides-component.module';
import { HomeBannerComponentModule } from 'src/app/components/home-banner/home-banner.component.module';
import { FormsModule } from '@angular/forms';
import { ShareBoxComponentModule } from 'src/app/components/share-box/share-box.component.module';
import { SharePageModule } from '../share/share.module';
import { PipesModule } from 'src/app/pipes/pipes.module';
@NgModule({
  declarations: [
    ItemListPage,
  ],
  imports: [
    RouterModule.forChild([
      {
        path: '',
        component: ItemListPage
      }
    ]),
    FormsModule,
    ItemBlockComponentsModule,
    CategoryImgSlidesComponentsModule,
    SharedModule,
    HomeBannerComponentModule,
    ShareBoxComponentModule,
    SharePageModule,
    PipesModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ItemListPageModule {}
