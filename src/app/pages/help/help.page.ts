import { Component, OnInit, Injector, ViewChild } from '@angular/core';
import { BasePage } from '../base-page/base-page';
import { FirebaseService } from 'src/app/services/fb/firebase.service';

import {
  trigger,
  style,
  animate,
  transition,
  query,
  stagger
} from '@angular/animations';
import { Subject, Observable, merge } from 'rxjs';
import { IonContent } from '@ionic/angular';


@Component({
  selector: 'app-help',
  templateUrl: './help.page.html',
  styleUrls: ['./help.page.scss'],
  animations: [
    trigger('staggerIn', [
      transition('* => *', [
        query(':enter', style({ opacity: 0, transform: `translate3d(0,10px,0)` }), { optional: true }),
        query(':enter', stagger('70ms', [animate('100ms', style({ opacity: 1, transform: `translate3d(0,0,0)` }))]), { optional: true })
      ])
    ])
  ]
})
export class HelpPage extends BasePage {
  @ViewChild(IonContent) container: IonContent;



  protected contentLoaded: Subject<any>;
  protected loadAndScroll: Observable<any>;
  public version: string;
  public about: any;
  public item: any;
  _title;
  enableMenuSwipe(): boolean {
    return true;
  }

  constructor(injector: Injector,
    public firebaseService: FirebaseService
  ) {
    super(injector);
    this.contentLoaded = new Subject();

  }

  ngOnInit() {
    this.setupObservable();
  }

  async ionViewDidEnter() {
    this.showLoadingView({ showOverlay: false });
    this.loadData();


    const title = await this.getTrans('SUPPORT');
    this._title = title;
    this.setPageTitle(title);

    this.setMetaTags({
      title: title
    });
  }

  setupObservable() {
    this.loadAndScroll = merge(
      this.container.ionScroll,
      this.contentLoaded
    );
  }
  async loadData(event: any = {}) {

    this.refresher = event.target;

    try {

      this.about = await this.firebaseService.getAppDetails();
      console.warn(this.about);
      this.about[0] = this.item;
      console.log(this.item);

      this.onRefreshComplete();
      this.showContentView();
      this.onContentLoaded();

    } catch (error) {
      this.translate.get('ERROR_NETWORK').subscribe((str) => this.showToast(str));
      this.onRefreshComplete();
      this.showContentView();
    }

  }

  onContentLoaded() {
    setTimeout(() => {
      this.contentLoaded.next();
    }, 400);
  }

  onSearch(value) {
    this.navigateTo(this.currentPath + '/items');
  }

}
