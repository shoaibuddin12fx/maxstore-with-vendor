import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { HelpPage } from './help.page';
import { SharedModule } from 'src/app/shared.module';
import { BlackHeaderComponentsModule } from 'src/app/components/black-header-component.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    HelpPage,
  ],
  imports: [
    RouterModule.forChild([
      {
        path: '',
        component: HelpPage
      }
    ]),
    BlackHeaderComponentsModule,
    SharedModule,
    TranslateModule
  ],
})
export class HelpPageModule {}
