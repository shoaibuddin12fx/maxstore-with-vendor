import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CategoryListPage } from './category-list';
import { SharedModule } from '../../shared.module';
import { BlackHeaderComponentsModule } from 'src/app/components/black-header-component.module';
import { CategoryImageBoxComponentsModule } from 'src/app/components/category-image-box/category-image-box.component.module';

@NgModule({
  declarations: [
    CategoryListPage,
  ],
  imports: [
    RouterModule.forChild([
      {
        path: '',
        component: CategoryListPage,
      }
    ]),
    BlackHeaderComponentsModule,
    CategoryImageBoxComponentsModule,
    SharedModule
  ],
})
export class CategoryListPageModule {}
