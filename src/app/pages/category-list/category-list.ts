import { Component, Injector, ViewChild } from '@angular/core';
import { IonContent } from '@ionic/angular';
import { Category } from '../../services/category';
import { BasePage } from '../base-page/base-page';
import { SubCategory } from '../../services/sub-category';
import { Subject, Observable, merge } from 'rxjs';
import {
  trigger,
  style,
  animate,
  transition,
  query,
  stagger
} from '@angular/animations';

@Component({
  selector: 'page-category-list',
  templateUrl: 'category-list.html',
  styleUrls: ['category-list.scss'],
  animations: [
    trigger('staggerIn', [
      transition('* => *', [
        query(':enter', style({ opacity: 0, transform: `translate3d(0,10px,0)` }), { optional: true }),
        query(':enter', stagger('40ms', [animate('100ms', style({ opacity: 1, transform: `translate3d(0,0,0)` }))]), { optional: true })
      ])
    ])
  ]
})
export class CategoryListPage extends BasePage {

  @ViewChild(IonContent) container: IonContent;

  public categories: any[] = [];
  public vendors: any[] = [];

  public searchText: string;
  public params: any = {};
  public skeletonArray = Array(16);
  public subcategories: any[] = [];
  public loadALlCats: boolean = true;
  public selectedCategory: any = {};
  protected contentLoaded: Subject<any>;
  protected loadAndScroll: Observable<any>;
  public selectedCategoryIndex = -1;
  public isVendors: boolean = false;

  constructor(injector: Injector) {
    super(injector);
    this.contentLoaded = new Subject();
  }

  enableMenuSwipe(): boolean {
    return false;
  }

  ngOnInit() {
    this.setupObservable();
  }

  async ionViewDidEnter() {

    if (!this.categories.length) {
      await this.showLoadingView({ showOverlay: false });
      this.ViewAllCategories();
      this.loadData();

    } else {
      this.onContentLoaded();
    }

    const title = await this.getTrans('CATEGORIES');
    this.setPageTitle(title);

    this.setMetaTags({
      title: title
    });
  }

  setupObservable() {

    this.loadAndScroll = merge(
      this.container.ionScroll,
      this.contentLoaded
    );

    this.events.subscribe('browse:changecat', this.loadSubcategoryById.bind(this))
  }

  loadSubcategoryById(obj) {
    let catid = obj['catid'];
    console.log("catid by event", catid);
    if (catid && this.categories.length > 0) {
      const catindex = this.categories.findIndex(x => x.id == catid);
      console.table(catid, catindex, this.categories[catindex])
      // this.selectedCategory['name'] = this.categories[catindex].name;
      this.loadSubcategoryData({}, catindex, { category: this.categories[catindex] });
    }

  }

  onContentLoaded() {
    setTimeout(() => {
      this.contentLoaded.next();
    }, 400);
  }

  async loadData(event: any = {}) {

    this.refresher = event.target;

    try {

      console.log(this.params);
      
      const catid = this.getQueryParams().catid;
      if (catid) {
        await this.showLoadingView({ showOverlay: false });
        // find sub category of category 
        this.subcategories  = await this.firebaseService.getSubCategoryListOfCategory(catid);
        console.log('subcategory', this.subcategories);

        if ( this.subcategories.length == 0 ) {
          this.navigateTo('1/browse/items', { catId: catid });
        }else{
          this.categories = [];
        }

        this.showContentView();

      }else{
        this.categories = await this.firebaseService.getCategoryList();
      }

      this.onRefreshComplete();
      this.showContentView();
      this.onContentLoaded();

    } catch (error) {
      this.translate.get('ERROR_NETWORK').subscribe((str) => this.showToast(str));
      this.onRefreshComplete();
      this.showContentView();
    }

  }

  ViewAllCategories() {
    this.loadALlCats = true;
    this.selectedCategory = {
      name: 'All Categories',
    };
    this.selectedCategoryIndex = -1
  }

  async loadSubcategoryData(event: any = {}, index, subcat) {

    this.loadALlCats = false;
    this.subcategories = [];
    console.log(index, subcat);
    this.selectedCategoryIndex = index;
    try {
      this.subcategories = await this.firebaseService.getSubCategoryListOfCategory(subcat.id);
      console.log(this.subcategories)
    } catch (error) {
      this.translate.get('ERROR_NETWORK').subscribe((str) => this.showToast(str));
    }

  }

  onSubCategoryTouched(subcategory: any) {
    this.navigateTo(this.currentPath + '/items', { subCatId:  subcategory.id });
  }

  onViewAll() {

    if(this.selectedCategoryIndex == -1){
      this.navigateTo(this.currentPath + '/items');
    }else{

      let category = this.categories[this.selectedCategoryIndex];
      this.navigateTo(this.currentPath + '/items', { catId:  category.id });
    }
    
  }

  async goToSubCategoryPage(category: any) {

    try {

        const count = (await this.firebaseService.getSubCategoryListOfCategory(category.id)).length;

        if (count) {
          this.navigateTo(this.currentPath + '/' + category.id);
        } else {
          this.navigateTo(this.currentPath + '/' + category.id + '/items');
        }

        this.showContentView();


    } catch (error) {
      this.showContentView();
      this.translate.get('ERROR_NETWORK').subscribe((str) => this.showToast(str));
    }

  }

  onSearch($event) {
    console.log($event);
    this.searchText = $event;
  }

  async onCategoryTouched(category: any) {

    try {

      await this.showLoadingView({ showOverlay: false });

      // find sub category of category 
      let subcategories  = await this.firebaseService.getSubCategoryListOfCategory(category.id);
      console.log(subcategories);
      if ( subcategories.length == 0 ) {
        this.navigateTo('1/browse/items', { catId: category.id });
      } else {
        this.navigateTo('1/browse/' + category.id, { catId: category.id });
      }




      




      this.showContentView();

    } catch (error) {
      this.showContentView();
      this.translate.get('ERROR_NETWORK').subscribe((str) => this.showToast(str));
    }

  }

}
