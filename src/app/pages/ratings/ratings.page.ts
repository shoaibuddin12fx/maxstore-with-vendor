import { Component, OnInit, Injector } from '@angular/core';
import { BasePage } from '../base-page/base-page';
import { FirebaseService } from 'src/app/services/fb/firebase.service';
import { FbUserService } from 'src/app/services/fb/fb-user.service';

@Component({
  selector: 'app-ratings',
  templateUrl: './ratings.page.html',
  styleUrls: ['./ratings.page.scss'],
})
export class RatingsPage extends BasePage implements OnInit {

  comment: any;
  rating: any = 4;
  itemId: any;
  pageTitle = 'Ratings Page';
  user: any;
  comments: any;

  totalRating;
  item;
  totalComments
  showComments = false;

  constructor(
    injector: Injector,
    public firebaseService: FirebaseService,
    public userService: FbUserService
  ) {
    super(injector);
  }

  ngOnInit() { }

  enableMenuSwipe(): boolean {
    return false;
  }

  async ionViewDidEnter() {
    try {
      await this.showLoadingView({ showOverlay: false });

      this.itemId = await this.getParams().itemId;
      this.user = await this.userService.getAuthUser();
      this.item = await this.firebaseService.getItemDetail(this.itemId);
      this.showContentView();

      this.comments = await this.firebaseService.getItemReview(this.itemId);
      if(this.comments) {
        this.showComments = true;
      }
      console.log(this.comments);
      console.log(this.user);
      console.log(this.itemId);

    } catch (error) {
      this.showErrorView();
    }
  }

  logRatingChange($event) {
    console.log($event);
    this.rating = $event;
    console.log(this.comment);
  }

  async submitComment() {
    let obj = {
      userId: this.user.uid,
      user_name: this.user.displayName,
      itemId: this.itemId,
      rating: this.rating,
      comment: this.comment,
      createdAt: this.firebaseService.getTimeStamp(),
    }
    const res = await this.firebaseService.addItemReview(obj);
    if (res) {
      this.comment = undefined;
      console.log('hrere');
      this.comments = await this.firebaseService.getItemReview(this.itemId);
    }
  }

  getRatingNumber(){
    if (this.item['comments'])  {
      this.totalRating= 0;
      Object.keys(this.item["comments"]).map(key => {
        let rat = this.item["comments"][key]['rating'];
        this.totalRating = this.totalRating + rat;
        this.totalComments = Object.keys(this.item["comments"]).length;
      });
      this.rating = Math.round(this.totalRating / this.totalComments);
      return this.rating;
    }else{
      return 0;
    }
  }

}
