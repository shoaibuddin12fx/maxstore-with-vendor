import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { RatingsPage } from './ratings.page';
import { BlackHeaderComponentsModule } from 'src/app/components/black-header-component.module';
import { StarRatingModule } from 'ionic4-star-rating';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild([
        {
          path: '',
          component: RatingsPage
        }
    ]),
    BlackHeaderComponentsModule,
    StarRatingModule,
  ],
  declarations: [RatingsPage]
})
export class RatingsPageModule {}
