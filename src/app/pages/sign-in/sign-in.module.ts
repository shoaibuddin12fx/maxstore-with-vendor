import { NgModule } from '@angular/core';
import { SignInPage } from './sign-in';
import { SharedModule } from '../../shared.module';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { ForgotPasswordPageModule } from '../forgot-password/forgot-password.module';
import { InternationalPhoneNumberModule } from 'ngx-international-phone-number';
import { TranslateModule } from '@ngx-translate/core';
import { BlackHeaderComponentsModule } from 'src/app/components/black-header-component.module';

@NgModule({
  declarations: [
    SignInPage,
  ],
  imports: [
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    ForgotPasswordPageModule,
    TranslateModule,
    BlackHeaderComponentsModule
    
  ],
  entryComponents: [
    
  ]
})
export class SignInPageModule {}
