import { Component, Injector, Input, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { BasePage } from '../base-page/base-page';
import { User } from '../../services/user';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook/ngx';
import { ForgotPasswordPage } from '../forgot-password/forgot-password';
import { environment } from './../../../environments/environment'
import { FbUserService } from 'src/app/services/fb/fb-user.service';
import * as firebase from 'firebase';
import { AlertController, ActionSheetController } from '@ionic/angular';
import { FirebaseService } from 'src/app/services/fb/firebase.service';
import { LocalStorage } from 'src/app/services/local-storage';

@Component({
  selector: 'page-sign-in',
  templateUrl: 'sign-in.html',
  styleUrls: ['sign-in.scss']
})
export class SignInPage extends BasePage implements OnInit {

  private form: FormGroup;
  public recaptchaVerifier: firebase.auth.RecaptchaVerifier;
  public backgroundImage = 'assets/imgs/loginbg/bg@2x.png';
  public logiImage = 'assets/imgs/logo-max-01.svg';
  public showPass = false;
  public language: any;
  @Input() showLoginForm: boolean;
  @Input() showSignUpForm: boolean;
  @Input() showOtpForm: boolean;
  @Input() showHeader: boolean = false;

  public isLoadingByUsername: boolean = false;
  public isLoadingByFacebook: boolean = false;
  public isLoadingByPhone: boolean = false;
  public isSignUpLoading: boolean = false;


  constructor(
    injector: Injector,
    public alertCtrl: AlertController,
    private fb: Facebook,
    private userService: User,
    public actionSheetController: ActionSheetController,
    private storage: LocalStorage
  ) {
    super(injector);
    // this.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container');
    this.showHeader = this.getQueryParams().showHeader;






  }

  ngOnInit() {
    // if (this.userService.isLoggedInViaPassword()) {
    //   let user = this .userService.getCurrent();
    //   this.events.publish('user:login', user);
    // }
    this.onOtpButtonTouched();
  }

  async checkLoggedIn() {
    let flag = await this.firebaseService.isAuthenticated();
  }



  enableMenuSwipe() {
    return false;
  }

  // onDismiss() {
  //   this.
  //   this.modalCtrl.dismiss();
  // }

  onLoginButtonTouched() {
    this.showLoginForm = true;
    this.showSignUpForm = false;
    this.showOtpForm = false;
    this.setupLoginForm();
  }

  onSignUpButtonTouched() {
    this.showLoginForm = false;
    this.showSignUpForm = true;
    this.showOtpForm = false;
    this.setupSignUpForm();
  }

  onOtpButtonTouched() {
    this.showLoginForm = false;
    this.showSignUpForm = false;
    this.showOtpForm = true;
    this.setupOtpForm();
  }

  setupOtpForm() {
    this.form = new FormGroup({
      phone: new FormControl('', Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(11)]))
      // password: new FormControl('', Validators.required)
    });
  }

  setupLoginForm() {
    this.form = new FormGroup({
      email: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required),
    });
  }

  setupSignUpForm() {
    this.form = new FormGroup({
      username: new FormControl('', Validators.required),
      phone: new FormControl('', Validators.required),
      email: new FormControl('', Validators.required),
      password: new FormControl('', [Validators.required, Validators.minLength(6)]),
    });
  }

  onFacebookButtonTouched() {

    if (!(this.isIos() || this.isAndroid())) {
      this.userService.loginViaFacebook()


      // .then((user: User) => this.loggedViaFacebook(user))
      // .catch(e => console.log('Error logging into Facebook', e));
    }
    //  else if (this.isPwa()) {
    //   this.translate.get('ERROR_FACEBOOK_PWA').subscribe((str: string) => this.showAlert(str));
    // } else {
    //   this.fb.login(['public_profile'])
    //   .then((res: FacebookLoginResponse) => this.loggedIntoFacebook(res))
    //   .catch(e => console.log('Error logging into Facebook', e));
    // }

  }

  updatePhoneNumber($event) {
    let v = $event.target.value;
    console.log(v);

    if (v.length > 11) {
      v = v.substring(0, 11);
    }

    this.form.controls.phone.setValue(v);


  }

  async onPhoneButtonTouched() {

    var self = this;
    if (this.form.controls.phone.invalid) {
      return;
    }

    this.isLoadingByPhone = true;
    firebase.auth().languageCode = this.preference.lang;

    let phoneNumber = '+964' + this.form.controls.phone.value;


    this.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container', {
      'size': 'invisible',
    });


    const appVerifier = this.recaptchaVerifier;
    const phoneNumberString = "+" + phoneNumber;


    await firebase.auth().setPersistence(firebase.auth.Auth.Persistence.LOCAL)
    firebase.auth().signInWithPhoneNumber(phoneNumberString, appVerifier)
      .then(async confirmationResult => {
        // SMS sent. Prompt user to type the code from the message, then sign the
        // user in with confirmationResult.confirm(code).

        const alert = await this.alertCtrl.create({
          cssClass: 'alrtcustomeclass',
          header: 'تم ارسال رسالة تفعيل',
          message: 'ادخل الرمز المستلم برقم هاتفك في المربع ادناه للدخول للتطبيق',

          inputs: [
            {
              name: 'otp',
              type: 'text',
              placeholder: '_ _ _ _ _ _',

            },

          ],
          buttons: [
            {
              text: 'الغاء',
              role: 'cancel',
              cssClass: 'alrtcancelbtn',
              handler: () => {
                console.log('Confirm Cancel');
                        self.isLoadingByPhone = false;

              }
            }, {
              text: 'تأكيد',
              cssClass: 'alrtconfirmbtn',
              handler: (data) => {
                console.log('Confirm Ok');

                console.log(data);

                confirmationResult
                  .confirm(data.otp)
                  .then(function (result) {
                    // User signed in successfully.
                    console.log(result.user);

                    self.userService.loginViaFirebasePhone(result.user.uid, phoneNumber);

                    // self.onDismiss();
                    self.events.publish('user:login', result.user);



                    self.isLoadingByPhone = false;
                    // ...
                  })
                  .catch(function (error) {
                    // User couldn't sign in (bad verification code?)
                    // ...
                    self.isLoadingByPhone = false;
                    console.log(error);
                  });




              }
            }
          ]
        });

        await alert.present();


      })
      .catch(function (error) {
        console.error("SMS not sent", error);

      });


    // if (!(this.isIos() || this.isAndroid())) {

    // this.userService.loginViaFirebasePhone(this.form.controls.phone.value)
    // .then((user: User) => this.loggedViaFacebook(user))
    // .catch(e => console.log('Error logging into Facebook', e));
    // } 

    // else if (this.isPwa()) {
    //   this.translate.get('ERROR_FACEBOOK_PWA').subscribe((str: string) => this.showAlert(str));
    // } else {
    //   this.fb.login(['public_profile'])
    //   .then((res: FacebookLoginResponse) => this.loggedIntoFacebook(res))
    //   .catch(e => console.log('Error logging into Facebook', e));
    // }

  }

  async loggedIntoFacebook(res: FacebookLoginResponse) {

    let expirationDate = new Date();
    expirationDate.setSeconds(expirationDate.getSeconds() + res.authResponse.expiresIn);

    let expirationDateFormatted = expirationDate.toISOString();

    var facebookAuthData = {
      id: res.authResponse.userID,
      access_token: res.authResponse.accessToken,
      expiration_date: expirationDateFormatted
    };

    try {

      await this.showLoadingView({ showOverlay: false });
      this.isLoadingByFacebook = true;

      // this.loggedViaFacebook(user);
      this.isLoadingByFacebook = false;

    } catch (error) {
      this.loginViaFacebookFailure(error);
      this.isLoadingByFacebook = false;
    }

  }

  loginViaFacebookFailure(error: any) {
    console.log('Error logging into Facebook', error);
    this.translate.get('ERROR_NETWORK').subscribe(str => this.showToast(str));
    this.showContentView();
  }

  loggedViaFacebook(user: User) {
    this.showContentView();

    const transParams = { name: user.name };

    this.translate.get('LOGGED_IN_AS', transParams)
      .subscribe(str => this.showToast(str));

    this.events.publish('user:login', user);

    // this.onDismiss();
  }

  async onLogin() {

    if (this.form.invalid) {
      const message = await this.getTrans('INVALID_FORM');
      return this.showToast(message);
    }

    await this.showLoadingView({ showOverlay: false });
    this.isLoadingByUsername = true;

    console.log(this.form.value);
    let user = await this.userService.signIn(this.form.value);

    if (!user) {
      const message = await this.getTrans('INVALID_CREDENTIALS');
      this.isLoadingByUsername = false;
      return this.showToast(message);

    }

    this.showContentView();
    this.isLoadingByUsername = false;

    // const transParams = { name: user['name'] };
    // this.translate.get('LOGGED_IN_AS', transParams)
    //   .subscribe(str => this.showToast(str));

    // this.onDismiss();

    this.events.publish('user:login', user);

  }

  async onSignUp() {

    if (this.form.invalid) {
      const message = await this.getTrans('INVALID_FORM');
      return this.showToast(message);
    }

    const formData = Object.assign({}, this.form.value);

    await this.showLoadingView({ showOverlay: false });
    this.isSignUpLoading = true;

    let user = await this.userService.create(formData);

    if (!user) {
      const message = await this.getTrans('INVALID_CREDENTIALS');
      this.isSignUpLoading = false;
      return this.showToast(message);
    }

    this.showContentView();
    this.isSignUpLoading = false;
    console.log(user);
    const transParams = { name: user.displayName };
    this.translate.get('LOGGED_IN_AS', transParams).subscribe(str => this.showToast(str));

    // this.onDismiss();

    this.events.publish('user:login', user);

  }

  async onForgotPasswordButtonTouched() {
    const modal = await this.modalCtrl.create({
      component: ForgotPasswordPage,
    });

    return await modal.present();
  }

  onSkipLogin() {
    this.router.navigate(['1/home']);
  }

  onChangeLang($event) {
    console.log($event);
    const lang = this.language;

    if (lang === 'ar') {
      document.dir = 'rtl';
    } else {
      document.dir = 'ltr';
    }

    this.storage.setLang(lang);
    this.translate.use(lang);
    this.preference.lang = lang;

  }
}