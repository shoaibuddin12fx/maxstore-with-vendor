import { Component, Injector, ViewChild } from '@angular/core';
import { IonSlides, IonContent } from '@ionic/angular';
import { BasePage } from '../base-page/base-page';
import { Item } from '../../services/item';
import { Cart, CartService } from '../../services/cart';
import { User } from '../../services/user';
import { SignInPage } from '../sign-in/sign-in';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { SharePage } from '../share/share.page';
import { Subject, Observable, merge } from 'rxjs';
import { ExchangeRateService } from 'src/app/services/exchange-rate.service';
import { Category } from 'src/app/services/category';
import { FavoritesService } from 'src/app/services/favorites.service';
import { AlertsService } from 'src/app/services/alerts.service';
import { IonSelect } from '@ionic/angular';
import { Router } from '@angular/router';
import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';

@Component({
  selector: 'page-item',
  templateUrl: 'item.html',
  styleUrls: ['item.scss']
})
export class ItemPage extends BasePage {

  // @ViewChild('slides') slides: IonSlides;
  @ViewChild(IonContent) content: IonContent;
  @ViewChild('colorSelect') colorSelect: IonSelect;
  @ViewChild('sizeSelect') sizeSelect: IonSelect;
  @ViewChild('deviceSelect') deviceSelect: IonSelect;


  aplha = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"];
  public item: Item;
  params;
  items = [];
  categoryId;
  subCategoryId;
  public itemDescription: any;
  public isAddingToCart: boolean = false;
  public isLiked: boolean = false;
  public itemId: any;
  public sizes: any[] = [];
  public colors: any[] = [];
  public devices: any[] = [];
  public selectedSize = '';
  public selectedColor = '';
  public selectedDevice = '';
  public rating: number = 5;
  public totalRating: any;
  public totalComments: any;
  public selectedSizePrice: any;

  colorActionSheetOptions: any = {
    header: 'Color - اللون',

  };

  sizeActionSheetOptions: any = {
    header: 'Size - الحجم',
  };

  deviceActionSheetOptions: any = {
    header: 'Device - الحجم',
  };
  
  public webSocialShare: { show: boolean, share: any, onClosed: any } = {
    show: false,
    share: {
      config: [{
        facebook: {
          socialShareUrl: '',
        },
      }, {
        twitter: {
          socialShareUrl: '',
        }
      }, {
        whatsapp: {
          socialShareText: '',
          socialShareUrl: '',
        }
      }]
    },
    onClosed: () => {
      this.webSocialShare.show = false;
    }
  };

  protected slidesEvent: Subject<any>;
  protected slidesObservable: Observable<any>;
  isLoadingViewVisible = true;
  featuredItems: any;
  cart: any;

  constructor(injector: Injector,
    private socialSharing: SocialSharing,
    public exrate: ExchangeRateService,
    public favService: FavoritesService,
    private cartService: CartService,
    private alert: AlertsService,
    public router: Router,
    public userService: User,
    private photoViewer: PhotoViewer) {
    super(injector);
    this.slidesEvent = new Subject();
  }

  enableMenuSwipe(): boolean {
    return false;
  }

  ngOnInit() {
    this.setupObservables();
  }

  setupObservables() {

    this.slidesObservable = merge(
      this.content.ionScroll,
      this.slidesEvent
    );
  }

  async ionViewDidEnter() {

    try {

      await this.showLoadingView({ showOverlay: false });


      this.itemId = await this.getParams().itemId;
      this.item = await this.firebaseService.getItemDetail(this.itemId);
      let count = await this.cartService.getCartQuantityOfProduct(this.item.id)
      this.item.qty = count;

      this.setSizeAndColors();

      // this.item.qty = 1;
      this.categoryId = this.item['categories'];
      this.subCategoryId = this.item['sub_category'];

      let dataItems = await this.firebaseService.getFeaturedItemByCategoryId(this.categoryId, 10);
      this.featuredItems = dataItems.filter(x => x.status === 'Active');

      this.itemDescription = this.sanitizer.bypassSecurityTrustHtml(this.item.description);
      // this.itemDescription = (!this.itemDescription['changingThisBreaksApplicationSecurity']) ? '' : this.itemDescription;
      this.setPageTitle(this.item.name);

      this.showContentView();
      this.checkIfItemIsLiked();
      this.loadItemsData();

      // this.setMetaTags({
      //   title: this.item.name,
      //   description: this.item.description,
      //   image: this.item.featuredImage.url(),
      //   slug: this.item.slug
      // });



    } catch (error) {
      this.showErrorView();
    }

  }

  getRatingNumber() {
    if (this.item['comments']) {
      this.totalRating = 0;
      Object.keys(this.item["comments"]).map(key => {
        let rat = this.item["comments"][key]['rating'];
        this.totalRating = this.totalRating + rat;
        this.totalComments = Object.keys(this.item["comments"]).length;

      });
      this.rating = Math.round(this.totalRating / this.totalComments);
      return this.rating;
    } else {
      return 0;
    }
  }

  setSizeAndColors() {


    console.log(this.item);

    if (this.item["size"]) {
      this.sizes = Object.keys(this.item["size"]).map(key => {
        return this.item["size"][key];
      });
    }


    if (this.item["color"]) {
      this.colors = Object.keys(this.item["color"]).map(key => {
        return this.item["color"][key];
      });
    }

    if (this.item.devices) {
      this.devices = Object.keys(this.item["devices"]).map(key => {
        return this.item["devices"][key];
      });
    }




  }

  onSlidesDidLoad() {
    this.slidesEvent.next();
  }

  onSlidesWillChange() {
    this.slidesEvent.next();
  }

  async presentSignInModal() {
    const modal = await this.modalCtrl.create({
      component: SignInPage,
      componentProps: {
        showLoginForm: true
      }
    });

    return await modal.present();
  }

  async checkIfItemIsLiked() {


    this.isLiked = await this.favService.isFavourit(this.item.id);


  }

  async onShare($event) {

    console.log($event);
    var base_url = window.location.origin;
    let url = 'https://maxstoreapp.com/1/home/items/' + this.item['id']
    switch ($event) {
      case 'web':
        break;
      case 'fb':
        url = 'fb-messenger://share/?link=' + url + '&app_id=291607058675731'
        break;
      case 'viber':
        url = 'viber://forward?text=' + url
        break;
      case 'whatsapp':
        url = 'https://wa.me/?text=' + url
        break;
      case 'telegram':
        url = 'https://telegram.me/share/url?url=' + url + '&text=' + this.item['name']
        break;
      default:
        break;
    }

    this.openUrlNoHttps(url)

    // if (this.isHybrid()) {

    //   try {
    //     const url = this.getShareUrl(this.item.id);
    //     const imgUrl = this.item['firebase_url'];
    //     await this.socialSharing.share(this.item.name, this.itemDescription, imgUrl, url);
    //   } catch (err) {
    //     console.warn(err);
    //   }

    // } else if (this.isPwa() || this.isMobile()) {
    //   this.webSocialShare.show = true;
    // } else {
    //   this.openShareModal();
    // }

  }

  async openShareModal() {
    const modal = await this.modalCtrl.create({
      component: SharePage,
      componentProps: {
        url: this.getShareUrl(this.item.slug)
      }
    });
    return await modal.present();
  }

  async onLike(liked) {

    this.isLiked = liked;
    if (!this.isLiked) {
      this.favService.removeFromFav(this.item.id)
    } else {
      this.favService.setIntoFav(this.item.id)
    }

  }

  async onAddToCart($event) {

    $event.stopPropagation();

    let flag = await this.userService.getCurrent();
    if (!flag) {
      this.router.navigate(['/signin'], { queryParams: { showHeader: true } });
    } else {
      if (this.item['size']) {
        if (!this.selectedSize) {
          this.sizeSelect.open();
        }
      }
      if (this.item['color']) {
        if (!this.selectedColor) {
          this.colorSelect.open();
        }
      }

      if (this.item['device']) {
        if (!this.selectedDevice) {
          this.deviceSelect.open();
        }
      }

      var self = this;
      this.item['selectedSize'] = this.selectedSize;
      this.item['selectedColor'] = this.selectedColor;
      this.item['selectedDevice'] = this.selectedDevice;
      this.cartService.onAddToCart(this.item).then(itm => {
        self.item['qty'] = itm.qty;
        this.alert.presentToast('Added to Cart Successfully!');
        let total = this.cartService.calculateTotal();
        let subtotal = this.cartService.calculateSubtotal();
      });
    }

  }

  onSearch(value) {
    this.navigateTo('1/home/items');
  }

  gotoRatings() {
    this.navigateTo('1/ratings/' + this.itemId);
  }
  async loadItemsData() {

    try {

      // check qty in cart
      let cart = this.cartService.get();
      let itm = cart.items.find(x => x.id == this.item.id);
      if (itm) {
        this.item.qty = itm.qty;
      } else {
        this.item.qty = 1;
      }

      // const val: string = 'a';
      // const canonical = (val && val.trim() != '') ? val.toLowerCase() : null;
      // this.items = [];
      // var category = new Category;
      // category.id = this.categoryId;
      // var params = {
      //   page: 0,
      //   limit: 10,
      //   category: category
      // };



      // this.items = await this.itemService.load(params);
      // console.log(this.items);

      // if (this.items.length) {
      //   this.showContentView();
      // } else {
      //   this.showEmptyView();
      // }

      //this.onRefreshComplete(items);

    } catch (error) {
      // this.showContentView();
      // this.onRefreshComplete();
      console.log(error);
      // this.translate.get('ERROR_NETWORK').subscribe((str) => this.showToast(str));
    }

  }

  goToItemPage(item: Item) {
    this.navigateTo('1/home/items/' + item.id);
  }

  incrementQuantity() {
    this.item.qty++;
  }

  decrementQuantity() {
    this.item.qty--;
    if (this.item.qty < 1) {
      this.item.qty = 1;
    }
  }

  onSizeChange($event) {
    let size = $event.target.value;

    if (size.size) {
      console.log(size);
      this.item.price = size['price'];
      this.selectedSize = size['size'];

      if (this.item.saleprice !== 0) {
        this.item.saleprice = size['prize'];
      }

    } else {
      this.selectedSize = size;
    }

  }

  onColorChange($event) {
    this.selectedColor = $event.target.value;
  }

  onDeviceChanged($event) {
    this.selectedDevice = $event.target.value;
  }

  openImage(src) {
    // $event.stopPropagation();
    console.log("Atleast this function is getting called")
    var options = {
      share: true, // default is false
      closeButton: false, // default is true
      copyToReference: true, // default is false
      headers: '',  // If this is not provided, an exception will be triggered
      piccasoOptions: {} // If this is not provided, an exception will be triggered
    };

    this.photoViewer.show(src, this.item.name, options);
  }
}
