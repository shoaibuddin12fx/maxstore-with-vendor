import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ItemPage } from './item';
import { SharedModule } from '../../shared.module';
import { SignInPageModule } from '../sign-in/sign-in.module';
import { SharePageModule } from '../share/share.module';
import { GalleryModule } from  '@ngx-gallery/core';
import { LightboxModule } from  '@ngx-gallery/lightbox';
import { GallerizeModule } from  '@ngx-gallery/gallerize';
import { ItemBlockComponentsModule } from 'src/app/components/item-block-component.module';
import { QtyCounterComponentsModule } from 'src/app/components/qty-counter-component.module';
import { TranslateModule } from '@ngx-translate/core';
import { ShareBoxComponentModule } from 'src/app/components/share-box/share-box.component.module';
import { StarRatingModule } from 'ionic4-star-rating';
import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';

@NgModule({
  declarations: [
    ItemPage,
  ],
  imports: [
    RouterModule.forChild([
      {
        path: '',
        component: ItemPage
      }
    ]),
    SharedModule,
    SignInPageModule,
    SharePageModule,
    GalleryModule,
    LightboxModule,
    GallerizeModule, 
    TranslateModule,
    ItemBlockComponentsModule,
    QtyCounterComponentsModule,
    ShareBoxComponentModule,
    StarRatingModule
  ],
  providers: [
    PhotoViewer
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ItemPageModule {}
