import { LocationCheckService } from 'src/app/services/location-check.service';
import { ExchangeRateService } from './../../services/exchange-rate.service';
import { Component, Injector, ViewChild } from '@angular/core';
import { IonSlides, IonContent } from '@ionic/angular';
import { BasePage } from '../base-page/base-page';
import { Slide } from '../../services/slide';
import { Item } from '../../services/item';
import * as Parse from 'parse';
import { Category } from '../../services/category';
import { SubCategory } from '../../services/sub-category';
import { Subject, Observable, merge } from 'rxjs';
import {
  trigger,
  style,
  animate,
  transition,
  query,
  stagger
} from '@angular/animations';
import { AddToCartService } from 'src/app/services/add-to-cart.service';
import { User } from 'src/app/services/user';
import { UniqueDeviceID } from '@ionic-native/unique-device-id/ngx';
import { environment } from 'src/environments/environment';
import { Banner } from 'src/app/services/banner';
import { SignInPage } from '../sign-in/sign-in';
import { ProductSearchService } from 'src/app/services/product-search.service';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  styleUrls: ['home.scss'],
  animations: [
    trigger('staggerIn', [
      transition('* => *', [
        query(':enter', style({ opacity: 0, transform: `translate3d(0,10px,0)` }), { optional: true }),
        query(':enter', stagger('70ms', [animate('100ms', style({ opacity: 1, transform: `translate3d(0,0,0)` }))]), { optional: true })
      ])
    ])
  ]
})
export class HomePage extends BasePage {

  @ViewChild(IonContent) content: IonContent;
  @ViewChild(IonSlides) ionSlides: IonSlides;

  public slidesConfig = {
    slidesPerView: 1,
    spaceBetween: 7,
    grabCursor: true,
    zoom: false,
    autoplayDisableOnInteraction: false,
    loop: true,
    speed: 500,
    breakpointsInverse: false,
    autoplay: {
      delay: 2000,
    },

    breakpoints: {
      992: {
        slidesPerView: 1,
        spaceBetween: 20,
        loop: false,
      },
    }
  };

  public mainBanners: any[] = [];
  public saleBanners: any[] = [];
  public featuredBanners: any[] = [];

  public skeletonArray = Array(6);
  public slides: any[] = [];
  public categories: any[] = [];
  public vendors: any[] = [];
  public featuredItems: any[] = [];

  public insertedcategories: Category[] = [];
  public subcategories: SubCategory[] = [];
  public itemsOnSale: Item[] = [];
  public itemsNewArrival: Item[] = [];
  public itemsFeatured: Item[] = [];
  public items: Item[] = [];
  public categoryBanners: Array<any> = [];

  items_pagination = '';

  private queryItems: any = {};

  protected contentLoaded: Subject<any>;
  protected loadAndScroll: Observable<any>;

  constructor(injector: Injector,
    private categoryService: Category,
    private subCategoryService: SubCategory,
    private bannerService: Banner,
    public atc: AddToCartService,
    public exrate: ExchangeRateService,
    public userService: User,
    private uniqueDeviceID: UniqueDeviceID,
    private locCheck: LocationCheckService,
    private itemService: Item,
    private search: ProductSearchService) {
    super(injector);
    this.contentLoaded = new Subject();
    this.events.subscribe('direct_product', this.directProduct.bind(this));
  }

  callToLogout(data) {
    // this.firebaseLogin()
  }

  enableMenuSwipe(): boolean {
    return true;
  }

  ngOnInit() {
    this.setupObservable();
    // this.locCheck.getLocation();
    this.events.subscribe('slider:startautoplay', this.pageReactivated.bind(this));
    this.events.subscribe('dashbaord:loaddata', this.loadData.bind(this));
    this.events.subscribe('user:loggedOut', this.callToLogout.bind(this));
    

  }

  directProduct(url){
    var self = this;
    console.log("Peep Peep", url)
    setTimeout( () => {
         

      if(url.includes('/1/home/items/')){                
        
        let str = url.split('1/home')[1];
        console.log(str);
        self.navigateTo(this.currentPath + str);
        
        
      }  
      
    }, 1000)
    
  }

  /**
 * Shuffles array in place. ES6 version
 * @param {Array} a items An array containing the items.
 */
  shuffle(a) {
    for (let i = a.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [a[i], a[j]] = [a[j], a[i]];
    }
    return a;
  }

  pageReactivated() {
    this.slides = this.shuffle(this.slides);

    if (this.ionSlides) {
      this.ionSlides.update();
      this.ionSlides.startAutoplay();
    }

  }

  openFeaturedItems(){
    this.navigateTo('1/browse/items', {featured:true});
  }

  async loginIfNot() {


    this.uniqueDeviceID.get()
      .then((uuid: any) => {
        console.log(uuid);
        this.anLogin(uuid);
      })
      .catch((error: any) => {
        console.log(error);
        var d = Date.now().toString();
        console.log(d);
        this.anLogin(d);
      });


  }

  private anLogin(uuid) {
    this.userService.anonymousLogin(uuid).then(user => {
      this.events.publish('user:login', user);
    });
  }

  async ionViewDidEnter() {
    if (!this.items.length) {
      this.showLoadingView({ showOverlay: false });
    }
    let products = await this.search.getAllProducts();
    this.itemsOnSale = products.filter(x => x.percent != 0);
    this.loadData();


    const title = await this.getTrans('APP_NAME');
    this.setPageTitle(title);

    this.setMetaTags({
      title: title
    });
    this.fireAutoPlay();
  }

  async removeAuth() {
    return this.firebaseService.logout();
  }


  ionViewWillLeave() {
    if (this.ionSlides) {
      this.ionSlides.stopAutoplay();
    }
  }

  ionViewWillEnter() {
    this.fireAutoPlay();
  }

  onSlidesDidLoad() {
    this.fireAutoPlay();
  }

  ionSlideTouchEnd($event) {
    this.fireAutoPlay();
  }

  fireAutoPlay() {
    if (this.ionSlides) {
      this.ionSlides.startAutoplay();
    }
  }



  setupObservable() {
    this.loadAndScroll = merge(
      this.content.ionScroll,
      this.contentLoaded
    );
  }

  onContentLoaded() {
    setTimeout(() => {
      this.contentLoaded.next();
    }, 400);
  }

  onSlideTouched(slide: Slide) {
    if (slide.item) {
      this.goToItemPage(slide.item);
    } else if (slide.url) {
      this.openUrl(slide.url);
    } else {
      // no action required
    }
  }

  async onAllCategoryTouched() {
    this.navigateTo('1/browse');
  }

  async onCategoryTouched(category: Category) {

    try {

      // if (category.subCategoryCount > 0) {
      //   this.navigateTo(this.currentPath + '/' + category.id, { catid: category.id  });
      // } else if (category.subCategoryCount === 0) {
      //   this.navigateTo(this.currentPath + '/' + category.id, { catid: category.id  });
      //   //this.navigateTo(this.currentPath + '/' + category.id + '/items', { title: category.name });
      // } else {

      await this.showLoadingView({ showOverlay: false });

      const count = await this.subCategoryService.count({
        category: category
      });

      this.navigateTo('1/browse/items', { catId: category['id'] });
      // setTimeout( () => {
      this.events.publish('browse:changecat', { catId: category['id'] });
      // }, 1000)

      // if (count) {
      //   this.navigateTo(this.currentPath + '/' + category.id, { catid: category.id  });
      // } else {
      //   this.navigateTo(this.currentPath + '/' + category.id, { catid: category.id  });
      //   // this.navigateTo(this.currentPath + '/' + category.id + '/items');
      // }

      this.showContentView();

      // }

    } catch (error) {
      this.showContentView();
      this.translate.get('ERROR_NETWORK').subscribe((str) => this.showToast(str));
    }

  }

  onViewAll(params: any = {}) {
    this.navigateTo('1/browse/items', params);
  }

  async loadData(event: any = {}) {

    this.refresher = event.target;

    // Parse.Cloud.run('getHomePageData').then(async data => {


    // this.slides = data.slides;
    await this.loadBanners();
    await this.loadCategoryData();
    await this.loadVendors();
    await this.loadFeaturedItems();
    // this.insertedcategories = await this.categoryService.load({});
    // this.itemsOnSale = data.itemsOnSale.slice(0, 9);
    // this.itemsNewArrival = data.itemsNewArrival.slice(0, 6);
    // this.itemsFeatured = data.itemsFeatured.slice(0, 9);;
    // this.loadAllSubCategories();
    // this.loadItems();



    this.onRefreshComplete();
    this.showContentView();

    this.onContentLoaded();

    if (this.ionSlides) {
      this.ionSlides.slideTo(0, 0);
      this.ionSlides.update();
    }

    this.fireAutoPlay();

    // }, () => {
    //   this.onRefreshComplete();
    //   this.showErrorView();
    // });

  }

  async loadVendors() {
    this.vendors = await this.firebaseService.getVendorList();
  }

  async loadCategoryData() {
    this.categories = await this.firebaseService.getCategoryList();
    this.insertedcategories = [...this.categories];
  }

  loadAllSubCategories() {

    for (var i = 0; i < this.categories.length; i++) {
      this.loadSubcategoryData(null, i, { category: this.categories[i] });
    }

  }

  async loadBanners() {

    return new Promise(async resolve => {

      this.slides = await this.firebaseService.getBannersList();
      console.log(this.slides);

      this.mainBanners = this.slides.filter(x => {
        return x.type == 'main_slider';
      });

      console.log(this.mainBanners);

      this.featuredBanners = this.slides.filter(x => {
        return x.type == 'featured_slider';
      });
      console.log(this.featuredBanners);

      // params['type'] = 'On sale';
      // this.saleBanners = await this.bannerService.load(params);


      // console.log(this.mainBanners, this.saleBanners, this.featuredBanners);
      resolve();

    });
  }

  async loadCategories() {
    return new Promise(async resolve => {
      this.categories = await this.firebaseService.GetCategoryList();
      console.log(this.ionSlides);

      resolve();
    });
  }

  async loadSubcategoryData(event: any = {}, index, subcat) {


    // console.log(index, subcat);

    try {
      const subcatresult = await this.subCategoryService.load(subcat);
      for (var i = 0; i < subcatresult.length; i++) {
        this.subcategories.push(subcatresult[i]);
      }

      // console.log("subcategory", subcatresult, this.subcategories);
    } catch (error) {
      //this.translate.get('ERROR_NETWORK').subscribe((str) => this.showToast(str));
    }

  }


  onLoadMore(event: any = {}) {

    this.infiniteScroll = event.target;
    this.queryItems.page++;
    this.loadItems();
  }

  async loadFeaturedItems() {

    let dataItems = await this.firebaseService.getfeturedList();
    this.featuredItems = dataItems.filter(x => x.status === 'Active');
    this.onRefreshComplete();

  }

  async loadItems() {


    if (this.items.length != 0) {
      this.items_pagination = this.items[this.items.length - 1]['id']
    }
    let dataItems = await this.firebaseService.getItemList(this.items_pagination);
    this.items = dataItems.filter(x => x.status === 'Active');

    this.onRefreshComplete();

  }

  onSearch(value) {
    console.log(value);
    this.navigateTo(this.currentPath + '/items', { search: value });
  }

  onSubCategoryTouched(subcategory: SubCategory) {
    console.log(subcategory);
    this.navigateTo('1/home' + '/' + subcategory.category.id + '/' + subcategory['id']);
  }

  // onSearch(event: any = {}) {

  //   const searchTerm = event.target.value;

  //   if (searchTerm) {
  //     this.navigateTo(this.currentPath + '/search/' + searchTerm);
  //   }

  // }

  goToItemPage(item: Item) {
    this.navigateTo(this.currentPath + '/items/' + item['id']);
  }

  addToCart($event, item: Item) {
    console.log("prevent?");
    $event.stopPropagation();
    this.atc.onAddToCart(item);
  }

  removeEmptyCategories($event) {
    console.log($event);
    const elementPos = this.insertedcategories.map(x => x.id).indexOf($event.id);
    console.log(elementPos);
    if (elementPos != -1) {
      this.insertedcategories.splice(elementPos, 1);
    }
  }

  async OnVendorTouched(vendor: any) {
    await this.showLoadingView({ showOverlay: false });
    this.navigateTo("/1/browse/items", { vendorId: vendor.id });
    this.showContentView();
  }

}
