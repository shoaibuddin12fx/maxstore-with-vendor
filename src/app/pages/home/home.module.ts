import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HomePage } from './home';
import { SharedModule } from '../../shared.module';
import { BlackHeaderComponentsModule } from 'src/app/components/black-header-component.module';
import { ItemBlockComponentsModule } from 'src/app/components/item-block-component.module';
import { CategorySlidesComponentsModule } from 'src/app/components/category-slides/category-slides-component.module';
import { HomeBannerComponentModule } from 'src/app/components/home-banner/home-banner.component.module';
import { CategoryImageBoxComponentsModule } from 'src/app/components/category-image-box/category-image-box.component.module';
import { CategoryComboBoxComponentsModule } from 'src/app/components/category-combo-box/category-combo-box.component.module';

@NgModule({
  declarations: [
    HomePage,
  ],
  imports: [
    RouterModule.forChild([
      {
        path: '',
        component: HomePage
      }
    ]),
    BlackHeaderComponentsModule,
    ItemBlockComponentsModule,
    HomeBannerComponentModule,
    SharedModule,
    CategorySlidesComponentsModule,
    CategoryImageBoxComponentsModule,
    CategoryComboBoxComponentsModule

  ],
})
export class HomePageModule { }
