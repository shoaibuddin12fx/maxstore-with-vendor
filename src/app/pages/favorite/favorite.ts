import { Component, Injector } from '@angular/core';
import { Item } from '../../services/item';
import { BasePage } from '../base-page/base-page';
import {
  trigger,
  style,
  animate,
  transition,
  query,
  stagger
} from '@angular/animations';
import { FavoritesService } from 'src/app/services/favorites.service';
@Component({
  selector: 'page-favorite',
  templateUrl: 'favorite.html',
  styleUrls: ['favorite.scss'],
  animations: [
    trigger('staggerIn', [
      transition('* => *', [
        query(':enter', style({ opacity: 0, transform: `translate3d(0,10px,0)` }), { optional: true }),
        query(':enter', stagger('100ms', [animate('300ms', style({ opacity: 1, transform: `translate3d(0,0,0)` }))]), { optional: true })
      ])
    ])
  ]
})
export class FavoritePage extends BasePage {

  showSearch = true;
  cangoback = false;
  private params: any = {
    likes: true,
    page: 0,
    limit: 40
  };
  public items: Item[] = [];
  public skeletonArray = Array(12);

  constructor(injector: Injector, private favService: FavoritesService) {
    super(injector);
    this.cangoback = this.getQueryParams().cangoback

  }

  enableMenuSwipe() {
    return true;
  }

  async ionViewDidEnter() {

    if (!this.items.length) {
      //this.showLoadingView({ showOverlay: false });
      this.showEmptyView();
      this.loadData();
    }
    
    const title = await this.getTrans('FAVORITES');
    this.setPageTitle(title);

    this.setMetaTags({
      title: title
    });
  }

  goToItemPage(item: Item) {
    this.navigateTo('1/browse/items/' + item.id);
  }

  async loadData() {

    

      let ids = await this.favService.getStorage();
      this.items = await this.firebaseService.getFavoriteItemByIds(ids);

      if (!this.items.length) {
        this.showEmptyView();
      } else {
        this.showContentView();
      }

    //   this.onRefreshComplete(this.items);

    // } catch (err) {
    //   this.showContentView();
    //   this.onRefreshComplete();
    //   this.translate.get('ERROR_NETWORK').subscribe((str) => this.showToast(str));
    // }

  }

  // onSearch(ev: any = {}) {
  //   const val: string = ev.target.value;
  //   const canonical = (val && val.trim() != '') ? val.toLowerCase() : null;
  //   this.params.canonical = canonical;
  //   this.params.page = 0;
  //   this.items = [];
  //   this.showLoadingView({ showOverlay: false });
  //   this.loadData();
  // }

  // onSearchCleared() {
  //   this.params.canonical = '';
  //   this.params.page = 0;
  //   this.items = [];
  //   this.showLoadingView({ showOverlay: false });
  //   this.loadData();
  // }

  onRefresh(event: any = {}) {
    this.refresher = event.target;
    this.items = [];
    this.params.page = 0;
    this.refresher.complete().then(()=>{
      this.loadData();
    })
  }

  // onLoadMore(event: any = {}) {
  //   this.infiniteScroll = event.target;
  //   this.params.page++;
  //   this.loadData();
  // }

  // onSearchToggle($event){
  //   console.log("boo")
  //   this.showSearch = !this.showSearch;
  // }

}
