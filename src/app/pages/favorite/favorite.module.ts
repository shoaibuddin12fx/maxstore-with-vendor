import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FavoritePage } from './favorite';
import { SharedModule } from '../../shared.module';
import { BlackHeaderComponentsModule } from 'src/app/components/black-header-component.module';
import { ItemBlockComponentsModule } from 'src/app/components/item-block-component.module';

@NgModule({
  declarations: [
    FavoritePage,
  ],
  imports: [
    RouterModule.forChild([
      {
        path: '',
        component: FavoritePage
      }
    ]),
    SharedModule,
    BlackHeaderComponentsModule,
    ItemBlockComponentsModule
  ],
})
export class FavoritePageModule {}
