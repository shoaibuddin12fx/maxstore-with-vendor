import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { OrderListPage } from './order-list-page';
import { SharedModule } from '../../shared.module';
import { BlackHeaderComponentsModule } from 'src/app/components/black-header-component.module';

@NgModule({
  declarations: [
    OrderListPage,
  ],
  imports: [
    RouterModule.forChild([
      {
        path: '',
        component: OrderListPage
      }
    ]),
    SharedModule,
    BlackHeaderComponentsModule
  ],
})
export class OrderListPageModule {}
