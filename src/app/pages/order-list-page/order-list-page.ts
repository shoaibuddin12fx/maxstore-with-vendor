import { Component, Injector } from '@angular/core';
import { BasePage } from '../base-page/base-page';
import { Order } from '../../services/order';
import { IonRefresher } from '@ionic/angular';
import { SignInPage } from '../sign-in/sign-in';

@Component({
  selector: 'page-order-list-page',
  templateUrl: 'order-list-page.html',
  styleUrls: ['order-list-page.scss']
})
export class OrderListPage extends BasePage {

  cangoback = false;
  public orders: Order[] = [];

  constructor(injector: Injector,
    private orderService: Order) {
    super(injector);

    this.cangoback = this.getQueryParams().cangoback

  }

  enableMenuSwipe(): boolean {
    return false;
  }

  async ionViewDidEnter() {
    if (!this.firebaseService.isAuthenticated()) {
      await this.removeAuth();
      await this.firebaseLogin()
    } else {
      if (!this.orders.length) {
        this.showLoadingView({ showOverlay: false });
        this.loadData();
      }
    }


    const title = await this.getTrans('MY_ORDERS');
    this.setPageTitle(title);

    this.setMetaTags({
      title: title
    });

  }

  async loadData(event: any = {}) {

    try {

      this.refresher = event.target;
      this.orders = await this.firebaseService.loadMyOrders();
      console.log(this.orders);
      if (this.orders.length) {
        this.showContentView();
      } else {
        this.showEmptyView();
      }

      this.onRefreshComplete(this.orders);

    } catch (error) {
      this.translate.get('ERROR_NETWORK').subscribe((str) => this.showToast(str));
      this.showContentView();
    }

  }

  onNavigateToOrderPage(order: Order) {
    this.navigateTo(this.currentPath + '/' + order.id);
  }

  onSearch(value) {
    this.navigateTo('1/home/items');
  }

  prettyItems(order) {
    return order.items.map(x => {
      return x.name
    }).join(', ')
  }

  async removeAuth() {
    return this.firebaseService.logout();
  }

  firebaseLogin() {

    console.log("pull")
    return new Promise(async resolve => {

      const modal = await this.modalCtrl.create({
        component: SignInPage,
      });

      modal.present();

      const { data } = await modal.onWillDismiss();

      if (data) {
        console.log(data);
        resolve(data);
      }
    })
  }
}
