import { Component, Injector } from '@angular/core';
import { BasePage } from '../base-page/base-page';
import { Order } from '../../services/order';
import { ExchangeRateService } from 'src/app/services/exchange-rate.service';
import { FirebaseService } from 'src/app/services/fb/firebase.service';

@Component({
  selector: 'page-order-detail-page',
  templateUrl: 'order-detail-page.html',
  styleUrls: ['order-detail-page.scss']
})
export class OrderDetailPage extends BasePage {

  public order: any;
  public vendors: any;
  constructor(
    injector: Injector, 
    private orderService: Order,
    public exrate: ExchangeRateService,
    public firebaseService: FirebaseService
    ) {
    super(injector);
  }

  async ionViewDidEnter() {

    try {

      await this.showLoadingView({ showOverlay: false });
  
      const orderId = await this.getParams().id;
      this.order = await this.orderService.loadOne(orderId);
      this.vendors = [];
      this.order['vendors'].forEach(async (vendor) => {
        let item = await this.firebaseService.getVendorListBtId(vendor.vendor_id);
        item['orderStatus'] = vendor.status;
        console.log(item);
        this.vendors.push(item);
      });
      console.log(this.vendors);
      console.log(this.order);
      this.showContentView();
      
    } catch (error) {
      this.showErrorView();
    }

  }

  enableMenuSwipe(): boolean {
    return false;
  }

  formatBrand() {

    if (this.order && this.order.card) {
      return this.order.card.brand.toLowerCase().replace(' ', '_')
    }

    return '';
    
  }

  onSearch(value) {
    this.navigateTo('1/home/items');
  }

  orderSubTotal(items){
      let t = items as any[];
      
      let subtotal = t.reduce((prevValue, item) => {
        return prevValue + item.saleprice;
      }, 0);

      return subtotal;

  }

  orderTotal(order){
    let subtotal = this.orderSubTotal(order.items) as number;
    return order.shipping + subtotal;
  }

}
