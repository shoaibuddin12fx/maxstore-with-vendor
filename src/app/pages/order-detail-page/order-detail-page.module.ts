import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { OrderDetailPage } from './order-detail-page';
import { SharedModule } from '../../shared.module';
import { BlackHeaderComponentsModule } from 'src/app/components/black-header-component.module';

@NgModule({
  declarations: [
    OrderDetailPage,
  ],
  imports: [
    RouterModule.forChild([
      {
        path: '',
        component: OrderDetailPage,
      }
    ]),
    SharedModule,
    BlackHeaderComponentsModule
  ],
  exports: [
    OrderDetailPage,

  ]
})
export class OrderDetailPageModule {}
