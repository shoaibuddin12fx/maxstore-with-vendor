import { Component, Injector, ViewChild, OnInit } from '@angular/core';
import { IonContent } from '@ionic/angular';
import { BasePage } from '../base-page/base-page';
import { Cart, CartService } from '../../services/cart';
import { User } from '../../services/user';
import { Item } from 'src/app/services/item';
import { Subject, Observable, merge } from 'rxjs';
import { ExchangeRateService } from 'src/app/services/exchange-rate.service';
@Component({
  selector: 'page-cart-page',
  templateUrl: 'cart-page.html',
  styleUrls: ['cart-page.scss']
})
export class CartPage extends BasePage implements OnInit {

  @ViewChild(IonContent) content: IonContent;
  public isSavingCart: boolean;

  protected contentLoaded: Subject<any>;
  protected loadAndScroll: Observable<any>;

  constructor(injector: Injector,
    public exrate: ExchangeRateService,
    public userService: User,
    public cartService: CartService) {

    super(injector);
    // this.events.subscribe('user:loggedOut', () => {
    //   this.cart = null;
    //   this.showEmptyView();
    // });

    // this.events.subscribe('user:loggedOut', () => {
    //   this.loadData();
    // });
    // this.cart = this.cartService.get();
    // this.loadData();
    // this.contentLoaded = new Subject();

  }

  enableMenuSwipe(): boolean {
    return true;
  }

  ngOnInit() {
    this.setupObservable();
  }

  async ionViewDidEnter() {

    if (this.firebaseService.isAuth()) {
      this.showLoadingView({ showOverlay: false });
      this.loadData();
    } else {
      this.showEmptyView();
    }

    const title = await this.getTrans('CART');
    this.setPageTitle(title);

    this.setMetaTags({
      title: title
    });
  }

  setupObservable() {
    this.loadAndScroll = merge(
      this.content.ionScroll,
      this.contentLoaded
    );
  }

  onContentLoaded() {
    // setTimeout(() => {
    //   this.contentLoaded.next();
    // }, 400);
  }

  async loadData(event: any = {}) {

    try {

      this.refresher = event.target;

      // this.cart = await this.cartService.getOne();
      console.log(this.cartService.cart)
      if (this.cartService.cart && this.cartService.cart.items.length != 0) {
        console.log(this.cartService.cart.items);
        this.showContentView();
      } else {
        this.showEmptyView();
      }

      this.onContentLoaded();

      this.onRefreshComplete(this.cartService.cart);

    } catch (error) {
      this.showContentView();
      this.translate.get('ERROR_NETWORK').subscribe(str => this.showToast(str));
    }
  }

  qtyChanged(item, qty){
    item.qty = qty;
    this.cartService.onAddToCart(item);
    // item.qty = parseInt(qty);
    // let p = parseInt(item.saleprice) > 0 ? item.saleprice : item.price;
    // item.amount = item.qty * p;

    // this.cartService.items
  }

  incrementQuantity(item: any) {
    
  }

  decrementQuantity(item: any) {

    if (item.qty > 1) {
      item.qty = item.qty - 1;
      let p = parseInt(item.saleprice) > 0 ? item.saleprice : item.price;
      item.amount = item.qty * p;
      this.cartService.onAddToCart(item)
    } else {
      this.onRemoveItem(item);
    }
  }

  calculateSubtotal(){
    this.cartService.calculateSubtotal();
  }



  async onRemoveItem(item: any) {

    try {

      let str = await this.getTrans('DELETE_CONFIRMATION');

      const res = await this.showConfirm(str);

      if (!res) return;

      await this.showLoadingView({ showOverlay: false });

      let index: number = this.cartService.cart.items.indexOf(item);
      if (index !== -1) {
        this.cartService.cart.items.splice(index, 1);
      }

      this.cartService.calculateSubtotal()

      // await this.cart.save();

       if (!this.cartService.cart.items.length) {
         this.showEmptyView();
       } else {
         this.showContentView();
       }

      this.events.publish('cart:updated', this.cartService.cart);

    } catch (error) {
      this.showContentView();
    }

  }

  goToItemPage(item: any) {
    console.log(item);
    this.navigateTo(this.currentPath + '/items/' + item.id);
  }

  async goToCheckout() {

    try {

      this.isSavingCart = true;
      // await this.cart.save();
      this.isSavingCart = false;

      this.navigateTo(this.currentPath + '/' + 'checkout');

    } catch (error) {
      this.isSavingCart = false;
      this.translate.get('ERROR_NETWORK').subscribe(str => this.showToast(str));
    }

  }

  onSearch(value) {
    this.navigateTo(this.currentPath + '/items');
  }

}
