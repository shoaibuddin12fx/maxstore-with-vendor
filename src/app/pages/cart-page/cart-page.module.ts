import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CartPage } from './cart-page';
import { SharedModule } from '../../shared.module';
import { QtyCounterComponentsModule } from 'src/app/components/qty-counter-component.module';

@NgModule({
  declarations: [
    CartPage,
  ],
  imports: [
    RouterModule.forChild([
      {
        path: '',
        component: CartPage
      }
    ]),
    SharedModule,
    QtyCounterComponentsModule
  ],
})
export class CartPageModule {}
