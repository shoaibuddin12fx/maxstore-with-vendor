import { Component, Injector } from '@angular/core';
import { Order } from '../../services/order';
import { BasePage } from '../base-page/base-page';
import { ExchangeRateService } from 'src/app/services/exchange-rate.service';
import { Cart, CartService } from '../../services/cart';
//import {CheckoutPage} from '../checkout-page/checkout-page';

@Component({
  selector: 'page-thanks-page',
  templateUrl: 'thanks-page.html',
  styleUrls: ['thanks-page.scss']
})
export class ThanksPage extends BasePage {

  public order: Order;
   private cart: Cart;
  constructor(injector: Injector, private orderService: Order, public exrate: ExchangeRateService, private cartService:CartService) {
    super(injector);
  }

  enableMenuSwipe(): boolean {
    return true;
  }

  async ionViewDidEnter() {

      await this.showLoadingView({ showOverlay: false });
      this.cart = this.cartService.get();
      const orderId = await this.getParams().orderId;
      this.order = await this.orderService.loadOne(orderId);
      this.showContentView();
      
      const title = await this.getTrans('THANK_YOU');
      this.setPageTitle(title);

      this.setMetaTags({
        title: title
      });
      //this.checkout.emptycart();
  }

  async goToHome() {
    await this.setRoot('/1/cart')
    this.emptycart();
    //this.navigateTo('/');
  }
  async emptycart(){
    this.cart.items.forEach(()=>{
      this.cart.items.pop();
    })
  }
}
