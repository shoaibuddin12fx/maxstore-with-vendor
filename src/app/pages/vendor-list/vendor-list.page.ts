import { Component, Injector, ViewChild } from '@angular/core';
import { IonContent } from '@ionic/angular';
import { Category } from '../../services/category';
import { BasePage } from '../base-page/base-page';
import { SubCategory } from '../../services/sub-category';
import { Subject, Observable, merge } from 'rxjs';
import {
  trigger,
  style,
  animate,
  transition,
  query,
  stagger
} from '@angular/animations';

@Component({
  selector: 'app-vendor-list',
  templateUrl: './vendor-list.page.html',
  styleUrls: ['./vendor-list.page.scss'],
  animations: [
    trigger('staggerIn', [
      transition('* => *', [
        query(':enter', style({ opacity: 0, transform: `translate3d(0,10px,0)` }), { optional: true }),
        query(':enter', stagger('40ms', [animate('100ms', style({ opacity: 1, transform: `translate3d(0,0,0)` }))]), { optional: true })
      ])
    ])
  ]
})
export class VendorListPage extends BasePage {

  @ViewChild(IonContent) container: IonContent;

  public categories: any[] = [];
  public vendors: any[] = [];

  public searchText: string;
  public params: any = {};
  public skeletonArray = Array(16);
  public subcategories: any[] = [];
  public loadALlCats: boolean = true;
  public selectedCategory: any = {};
  protected contentLoaded: Subject<any>;
  protected loadAndScroll: Observable<any>;
  public selectedCategoryIndex = -1;

  constructor(injector: Injector,
    private categoryService: Category,
    private subCategoryService: SubCategory) {
    super(injector);
    this.contentLoaded = new Subject();
  }

  enableMenuSwipe(): boolean {
    return false;
  }

  ngOnInit() {
    this.setupObservable();
  }

  async ionViewDidEnter() {

    if (!this.categories.length) {
      await this.showLoadingView({ showOverlay: false });
      this.ViewAllCategories();
      this.loadData();

    } else {
      this.onContentLoaded();
    }

    const title = await this.getTrans('CATEGORIES');
    this.setPageTitle(title);

    this.setMetaTags({
      title: title
    });
  }

  setupObservable() {
    this.loadAndScroll = merge(
      this.container.ionScroll,
      this.contentLoaded
    );

    this.events.subscribe('browse:changecat', this.loadSubcategoryById.bind(this))
  }

  loadSubcategoryById(obj) {
    let catid = obj['catid'];
    console.log("catid by event", catid);
    if (catid && this.categories.length > 0) {
      const catindex = this.categories.findIndex(x => x.id == catid);
      console.table(catid, catindex, this.categories[catindex])
      // this.selectedCategory['name'] = this.categories[catindex].name;
      this.loadSubcategoryData({}, catindex, { category: this.categories[catindex] });
    }

  }

  onContentLoaded() {
    setTimeout(() => {
      this.contentLoaded.next();
    }, 400);
  }

  async loadData(event: any = {}) {

    this.refresher = event.target;

    try {

      console.log(this.params);
      let vendorData = await this.firebaseService.getVendorList();
      this.vendors  = vendorData.filter(x => x.status = "Active");
      console.log(this.vendors);
      // const catid = this.getQueryParams().catid;
      // if (catid) {
      //   const catindex = this.categories.findIndex(x => x.id == catid);
      //   console.table(catid, catindex, this.categories[catindex])
      //   this.selectedCategory['cat_name'] = this.categories[catindex].cat_name;
      //   // this.loadSubcategoryData({}, catindex, { category: this.categories[catindex] });
      // }

      this.onRefreshComplete();
      this.showContentView();
      this.onContentLoaded();

    } catch (error) {
      this.translate.get('ERROR_NETWORK').subscribe((str) => this.showToast(str));
      this.onRefreshComplete();
      this.showContentView();
    }

  }

  ViewAllCategories() {
    this.loadALlCats = true;
    this.selectedCategory = {
      name: 'All Categories',
    };
    this.selectedCategoryIndex = -1
  }

  async loadSubcategoryData(event: any = {}, index, subcat) {

    this.loadALlCats = false;
    this.subcategories = [];
    console.log(index, subcat);
    this.selectedCategoryIndex = index;
    if (subcat.category) {
      this.selectedCategory = subcat.category
    }

    try {
      this.subcategories = await this.subCategoryService.load(subcat);
    } catch (error) {
      //this.translate.get('ERROR_NETWORK').subscribe((str) => this.showToast(str));
    }

  }

  onSubCategoryTouched(subcategory: SubCategory) {
    this.navigateTo('1/home' + '/' + this.selectedCategory.id + '/' + subcategory.id);
  }

  onViewAll() {
    // if(this.selectedCategory.id){
    //   this.navigateTo('/1/home/' + this.selectedCategory.id + '/items');
    // }else{
    //   this.navigateTo('/1/home/items');
    // }

    this.navigateTo('/1/browse/items');

  }

  async goToSubCategoryPage(category: Category) {

    try {

      if (category.subCategoryCount > 0) {
        this.navigateTo(this.currentPath + '/' + category.id);
      } else if (category.subCategoryCount === 0) {
        this.navigateTo(this.currentPath + '/' + category.id + '/items', { title: category.name });
      } else {

        await this.showLoadingView({ showOverlay: false });

        const count = await this.subCategoryService.count({
          category: category
        });

        if (count) {
          this.navigateTo(this.currentPath + '/' + category.id);
        } else {
          this.navigateTo(this.currentPath + '/' + category.id + '/items');
        }

        this.showContentView();

      }

    } catch (error) {
      this.showContentView();
      this.translate.get('ERROR_NETWORK').subscribe((str) => this.showToast(str));
    }

  }

  onSearch($event) {
    console.log($event);
    this.searchText = $event;
  }

  async onCategoryTouched(category: any) {

    try {

      // if (category.subCategoryCount > 0) {
      //   this.navigateTo(this.currentPath + '/' + category.id, { catid: category.id  });
      // } else if (category.subCategoryCount === 0) {
      //   this.navigateTo(this.currentPath + '/' + category.id, { catid: category.id  });
      //   //this.navigateTo(this.currentPath + '/' + category.id + '/items', { title: category.name });
      // } else {

      await this.showLoadingView({ showOverlay: false });

      // const count = await this.subCategoryService.count({
      //   category: category
      // });

      this.navigateTo('1/browse/items', { catId: category.id });
      // setTimeout( () => {
      // this.events.publish('browse:changecat', { catId: category.id });
      // }, 1000)

      // if (count) {
      //   this.navigateTo(this.currentPath + '/' + category.id, { catid: category.id  });
      // } else {
      //   this.navigateTo(this.currentPath + '/' + category.id, { catid: category.id  });
      //   // this.navigateTo(this.currentPath + '/' + category.id + '/items');
      // }

      this.showContentView();

      // }

    } catch (error) {
      this.showContentView();
      this.translate.get('ERROR_NETWORK').subscribe((str) => this.showToast(str));
    }

  }

  async OnVendorTouched(vendor: any){
    await this.showLoadingView({showOverlay: false});
    this.navigateTo("/1/browse/items",{vendorId: vendor.id});
    this.showContentView();
  }

}
