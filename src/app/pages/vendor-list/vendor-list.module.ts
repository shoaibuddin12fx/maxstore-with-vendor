import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { VendorListPage } from './vendor-list.page';
import { BlackHeaderComponentsModule } from 'src/app/components/black-header-component.module';
import { SharedModule } from 'src/app/shared.module';

const routes: Routes = [
  {
    path: '',
    component: VendorListPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    BlackHeaderComponentsModule,
    SharedModule

  ],
  declarations: [VendorListPage]
})
export class VendorListPageModule {}
